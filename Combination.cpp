#include "Combination.h"

void print_key(COMBINATION * C)
{
  printf("Combination = [");
  for(int i=0;i<C->len;i++)
  {
    printf(" %d(%s) ",C->snps[i]->id,C->snps[i]->name);
  }
  printf("]\n");
}

void print(COMBINATION * C)
{
  printf("Combination = [");
  for(int i=0;i<C->len;i++)
  {
    printf(" %d(%s) ",C->snps[i]->id,C->snps[i]->name);
  }
  //printf("]\tpai = %g pvalue=%g key=[%s] hash=%lld",C->pai,C->pai_pvalue,C->key->c_str(),C->hashid);
  printf("]\tpai = %g pvalue=%g ",C->pai,C->pai_pvalue);
  printf("  Parents = [");
  for(int i=0;i<C->parents_len;i++)
      printf("%d,",C->parents[i]);
  printf("]");
  //printf("\tlast_snp_index = %d",C->last_snp_index);
  printf("\n");
}

COMBINATION * combination()
{
   //cout<<"               COMB     11    MALLOC\n"; 
   COMBINATION * C = (COMBINATION *)malloc(sizeof(COMBINATION));
   C->key=new string("");
   C->comb_str = (int*) malloc(MAX_COMB_SIZE*sizeof(int));
   C->kwii = 0;
   C->pai = 0;
   C->entropy1 = 0;
   C->entropy2 = 0;
   C->snps = (SNP_type **)malloc(MAX_COMB_SIZE*sizeof(SNP_type*));
   C->table = NULL;
   C->len = 0;
   C->pai_pvalue = 1.0;
   C->kwii_pvalue = 1.0;
   C->last_snp_index = -1;
   C->last_but_one_snp_index = -1;
   C->hashid = -1;
   memset(C->parents,0,MAX_COMB_SIZE);
   C->parents_len = 0;
   return C;
}

//copy C1 <- C2, allocate memory for new table in C1
COMBINATION * combination(COMBINATION * C) 
{
   //cout<<"               COMB     2    MALLOC\n"; 
   COMBINATION * C1 = (COMBINATION *)malloc(sizeof(COMBINATION));
   C1->key = new string(C->key->c_str());
   C1->comb_str = (int*) malloc(MAX_COMB_SIZE*sizeof(int));
   memcpy(C1->comb_str,C->comb_str,MAX_COMB_SIZE*sizeof(int));
   C1->kwii = C->kwii;
   C1->pai = C->pai;
   C1->entropy1 = C->entropy1;
   C1->entropy2 = C->entropy2;
   C1->snps = (SNP_type **)malloc(MAX_COMB_SIZE*sizeof(SNP_type*));
   memcpy(C1->snps,C->snps,MAX_COMB_SIZE*sizeof(SNP_type*));
   C1->table = table(C->table);//copy of C->table, will malloc TABLE
   C1->len = C->len;
   C1->pai_pvalue = C->pai_pvalue;
   C1->kwii_pvalue = C->kwii_pvalue;
   C1->last_snp_index = C->last_snp_index;
   C1->last_but_one_snp_index = C->last_but_one_snp_index;
   C1->hashid = C->hashid;
   memcpy(C1->parents,C->parents,MAX_COMB_SIZE);
   C1->parents_len = C->parents_len;
   return C1;
}

//copy C1 <- C2, do not allocate memory for table.
void combination_overwrite(COMBINATION * C1,COMBINATION * C2) 
{
   //cout<<"               COMB     3    overwrite\n"; 
   memcpy(C1->comb_str,C2->comb_str,MAX_COMB_SIZE*sizeof(int));
   C1->key->assign(C2->key->c_str());
   C1->kwii = C2->kwii;
   C1->pai = C2->pai;
   C1->entropy1 = C2->entropy1;
   C1->entropy2 = C2->entropy2;
   memcpy(C1->snps,C2->snps,MAX_COMB_SIZE*sizeof(SNP_type*));
   table(C1->table,C2->table);//copy of C2->table.
   C1->len = C2->len;
   C1->pai_pvalue = C2->pai_pvalue;
   C1->kwii_pvalue = C2->kwii_pvalue;
   C1->last_snp_index = C2->last_snp_index;
   C1->last_but_one_snp_index = C2->last_but_one_snp_index;
   C1->hashid = C2->hashid;
   memcpy(C1->parents,C2->parents,MAX_COMB_SIZE);
   C1->parents_len = C2->parents_len;
}

void make_key(COMBINATION * C)
{
   static char s[MAX_COMB_CHARS] = "";
   for(int i=0;i<C->len;i++)
   {
      if(i==0)
         sprintf(s,"%d",C->snps[i]->id);
      else
         sprintf(s,"%s:%d",s,C->snps[i]->id);
   }
   C->key->assign(s);
}

void append(COMBINATION * C, SNP_type * u) 
{
    int pos = -1;
    //printf("Appending snp = %d\n",u->id); 
    for(int i=0;i<C->len;i++)
    {
      if(u->id == C->comb_str[i])
      {
         //duplicate.
         return;
      }
      if(u->id < C->comb_str[i])
      {
        pos = i; 
        break;
      }
    }
    //printf("Got pos = %d\n",pos);
    if(pos>=0)
    {
      //push up everything else.
      for(int j=C->len;j>pos;j--)
      {
         C->comb_str[j] = C->comb_str[j-1];
         C->snps[j] = C->snps[j-1];
      }
      //and insert.
      C->comb_str[pos] = u->id;
      C->snps[pos] = u;
    }
    else //add to end.
    {
       pos = C->len;
       C->comb_str[pos] = u->id;
       C->snps[pos] = u;
    }
    C->len++;
    C->last_but_one_snp_index = C->last_snp_index;
    C->last_snp_index = pos;
}

void remove(COMBINATION * C, SNP_type * u)
{
    int pos = -1;
    //printf("Removing snp = %d\n",u->id); 
    for(int i=0;i<C->len;i++)
    {
      if(u->id == C->comb_str[i])
      {
         pos = i;
         break;
      }
    }
    //printf("Got pos = %d\n",pos);
    if(pos>=0)
    {
      //push down everything else.
      for(int j=pos;j<C->len-1;j++)
      {
         C->comb_str[j] = C->comb_str[j+1];
         C->snps[j] = C->snps[j+1];
      }
      C->len--;
      C->last_snp_index = C->last_but_one_snp_index;
      C->last_but_one_snp_index = -1;
    }
}

void free_combination(COMBINATION * C)
{
   free(C->comb_str);
   free(C->snps);
   free_table(C->table);
}

