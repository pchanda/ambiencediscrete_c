#include "FastEpi.h"

char *itob(chunk_t x)
{
  static char buff[CHUNK_SZ + 1];
  int i;
  int j = CHUNK_SZ - 1;

  buff[j] = 0;
  for(i=0;i<CHUNK_SZ; i++)
  {
     if(x & ((chunk_t)1 << i))
        buff[j] = '1';
     else
        buff[j] = '0';
     j--;
  }
  return buff;
}

chunk_t compress(int* x,int n)
{
  int i = 0;
  chunk_t c=0;
  for(i=0;i<n;i++)
  { 
     chunk_t w;
     if(x[i]==1) 
     {
        w = (chunk_t) 1<<(CHUNK_SZ-1-i);
        c |= w;
     }
  }
  return c;
}

void compress_snp(int* x,SNP_type* XB)
{
   //X should be 0/1/2 vector.
   int x0[CHUNK_SZ];
   int x1[CHUNK_SZ];
   int x2[CHUNK_SZ];
   int i=0;
   int j = 0;
   int k = 0;
   int N = XB->nsample;
   for(i=0;i<N;i++)
   {
      if(i % CHUNK_SZ == 0 && i>0)
      {
         //printf("(%d) compressing vector of length = %d\n",k,j);
         XB->C0[k] = compress(x0,j);
         XB->C1[k] = compress(x1,j);
         XB->C2[k] = compress(x2,j);
         k++;
         j = 0;
         if(x[i]==0) x0[j] = 1; else x0[j] = 0;
         if(x[i]==1) x1[j] = 1; else x1[j] = 0;
         if(x[i]==2) x2[j] = 1; else x2[j] = 0;
         j++;
      }
      else if(i==N-1)
      {
         if(x[i]==0) x0[j] = 1; else x0[j] = 0;
         if(x[i]==1) x1[j] = 1; else x1[j] = 0;
         if(x[i]==2) x2[j] = 1; else x2[j] = 0;
         j++;
         //printf("(%d) compressing vector of length = %d\n",k,j);
         XB->C0[k] = compress(x0,j);
         XB->C1[k] = compress(x1,j);
         XB->C2[k] = compress(x2,j);
         k++;
      }
      else
      {
         if(x[i]==0) x0[j] = 1; else x0[j] = 0;
         if(x[i]==1) x1[j] = 1; else x1[j] = 0;
         if(x[i]==2) x2[j] = 1; else x2[j] = 0;
         j++;
      }
   }
}

void compress_pheno(PHENOTYPE* P)
{
   //X should be 0/1/2 vector.
   int x0[CHUNK_SZ];
   int x1[CHUNK_SZ];
   int i=0;
   int j = 0;
   int k = 0;
   int N = P->nsample;
   int * x = P->data;
   for(i=0;i<N;i++)
   {
      if(i % CHUNK_SZ == 0 && i>0)
      {
         //printf("(%d) compressing vector of length = %d\n",k,j);
         P->C0[k] = compress(x0,j);
         P->C1[k] = compress(x1,j);
         k++;
         j = 0;
         if(x[i]==0) x0[j] = 1; else x0[j] = 0;
         if(x[i]==1) x1[j] = 1; else x1[j] = 0;
         j++;
      }
      else if(i==N-1)
      {
         if(x[i]==0) x0[j] = 1; else x0[j] = 0;
         if(x[i]==1) x1[j] = 1; else x1[j] = 0;
         j++;
         //printf("(%d) compressing vector of length = %d\n",k,j);
         P->C0[k] = compress(x0,j);
         P->C1[k] = compress(x1,j);
         k++;
      }
      else
      {
         if(x[i]==0) x0[j] = 1; else x0[j] = 0;
         if(x[i]==1) x1[j] = 1; else x1[j] = 0;
         j++;
      }
   }
}

void print(SNP_type * XB)
{
  int i = 0;
  /*
  printf("geno = ");
  for(i=0;i<XB->nsample;i++)
  {
     printf("%d",XB->geno[i]);
  }
  printf("\n");
  */
  printf("X0   = ");
  for(i=0;i<n_chunk;i++)
  {
     char* C0 = itob(XB->C0[i]);
     printf("%s ",C0);
  }
  printf("\n");  
  printf("X1   = ");
  for(i=0;i<n_chunk;i++)
  {
     char* C1 = itob(XB->C1[i]);
     printf("%s ",C1);
  }
  printf("\n");  
  printf("X2   = ");
  for(i=0;i<n_chunk;i++)
  {
     char* C2 = itob(XB->C2[i]);
     printf("%s ",C2);
  }
  printf("\n-----------------------------------------------------------\n");  
}
