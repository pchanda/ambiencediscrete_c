#include "Table.h"

int popcount_31(chunk_t* C)
{
    int cnt = 0;
    for(int i=0;i<n_chunk;i++)
    {
      chunk_t x = C[i];
      x -= (x >> 1) & m1;             //put count of each 2 bits into those 2 bits
      x = (x & m2) + ((x >> 2) & m2); //put count of each 4 bits into those 4 bits 
      x = (x + (x >> 4)) & m4;        //put count of each 8 bits into those 8 bits 
      cnt += (x * h01)>>56;  //returns left 8 bits of x + (x<<8) + (x<<16) + (x<<24) + ...
    }
    return cnt;
}

int popcount_3(chunk_t x)
{
    x -= (x >> 1) & m1;             //put count of each 2 bits into those 2 bits
    x = (x & m2) + ((x >> 2) & m2); //put count of each 4 bits into those 4 bits 
    x = (x + (x >> 4)) & m4;        //put count of each 8 bits into those 8 bits 
    return (x * h01)>>56;  //returns left 8 bits of x + (x<<8) + (x<<16) + (x<<24) + ... 
}

int AND_count_2(chunk_t* Ca, chunk_t* Cb)
{
  int cnt = 0;
  for(int i=0;i<n_chunk;i++)
  {
     cnt += popcount_3(Ca[i] & Cb[i]);
  }
  return cnt;
}

int AND_count_3(chunk_t* Ca, chunk_t* Cb, chunk_t* Cc)
{
  int cnt = 0;
  for(int i=0;i<n_chunk;i++)
  {
     cnt += popcount_3(Ca[i] & Cb[i] & Cc[i]);
  }
  return cnt;
}

//create a copy of table t
TABLE * table(TABLE * t)
{ 
   //cout<<"                 TABLE       MALLOC\n"; 
   TABLE * Tab = (TABLE*)malloc(sizeof(TABLE));
   //copy T
   Tab->dim_T[0] = t->dim_T[0];
   Tab->dim_T[1] = t->dim_T[1];
   Tab->T_dim_true[0] = t->T_dim_true[0];
   Tab->T_dim_true[1] = t->T_dim_true[1];

   if(Tab->dim_T[0] > Tab->T_dim_true[0] || Tab->dim_T[1] > Tab->T_dim_true[1]) 
   {
      cout<<"Error : Table true dimensions "<<Tab->T_dim_true[0]<<","<<Tab->T_dim_true[1]<<" less than "<<Tab->dim_T[0]<<","<<Tab->dim_T[1]<<endl;
      exit(1);
   }

   Tab->T = (int**)malloc(Tab->T_dim_true[0]*sizeof(int*));
   for(int i=0;i<Tab->T_dim_true[0];i++)
   {
      Tab->T[i] = (int*)malloc(Tab->T_dim_true[1]*sizeof(int));
      for(int j=0;j<Tab->T_dim_true[1];j++)
        Tab->T[i][j] = t->T[i][j];
   }
   //copy P
   Tab->dim_P[0] = t->dim_P[0];
   Tab->dim_P[1] = t->dim_P[1];
   Tab->P_dim_true[0] = t->P_dim_true[0];
   Tab->P_dim_true[1] = t->P_dim_true[1];

   Tab->P = (int**)malloc(Tab->P_dim_true[0]*sizeof(int*));
   for(int i=0;i<Tab->P_dim_true[0];i++)
   {
      Tab->P[i] = (int*)malloc(Tab->P_dim_true[1]*sizeof(int));
      for(int j=0;j<Tab->P_dim_true[1];j++)
        Tab->P[i][j] = t->P[i][j];
   }
   //copy rowsum
   Tab->rowsum = (int*)malloc(Tab->P_dim_true[0]*sizeof(int));
   memcpy(Tab->rowsum,t->rowsum,Tab->P_dim_true[0]*sizeof(int));   
   //copy colsum
   Tab->colsum = (int*)malloc(Tab->P_dim_true[1]*sizeof(int));
   memcpy(Tab->colsum,t->colsum,Tab->P_dim_true[1]*sizeof(int));   
   return Tab;
}

//copy content of table t2 to t1.
//t1 <- t2
void table(TABLE * t1, TABLE * t2)
{ 
   //printf("copying table 1 <- table 2 : true dimensions T1(%d %d),P1(%d %d) , T2(%d %d),P2(%d %d)\n",
   //        t1->T_dim_true[0], t1->T_dim_true[1], t1->P_dim_true[0], t1->P_dim_true[1],
   //        t2->T_dim_true[0], t2->T_dim_true[1], t2->P_dim_true[0], t2->P_dim_true[1]);
   
   //copy t2 to t1.
   //t1 should be at least as big as t2.
   //true dimensions of t1 should not change.
   if(t1->T_dim_true[0] < t2->T_dim_true[0] || t1->T_dim_true[1] < t2->T_dim_true[1] || t1->P_dim_true[1] < t2->P_dim_true[1])
   {
      printf("Error copying table 2 to table 1 : true dimensions T1(%d %d),P1(%d %d) < T2(%d %d),P2(%d %d)\n",
             t1->T_dim_true[0], t1->T_dim_true[1], t1->P_dim_true[0], t1->P_dim_true[1],
             t2->T_dim_true[0], t2->T_dim_true[1], t2->P_dim_true[0], t2->P_dim_true[1]);
      exit(1); 
   }

   t1->dim_T[0] = t2->dim_T[0];
   t1->dim_T[1] = t2->dim_T[1];

   //Do not copy true dimensions. size t1 should be >= size t2
   //t1->T_dim_true[0] = t2->T_dim_true[0];
   //t1->T_dim_true[1] = t2->T_dim_true[1];

   for(int i=0;i<t2->T_dim_true[0];i++)
      for(int j=0;j<t2->T_dim_true[1];j++)
        t1->T[i][j] = t2->T[i][j];
   //copy P
   t1->dim_P[0] = t2->dim_P[0];
   t1->dim_P[1] = t2->dim_P[1];

   //Do not copy true dimensions. size t1 should be >= size t2
   //t1->P_dim_true[0] = t2->P_dim_true[0];
   //t1->P_dim_true[1] = t2->P_dim_true[1];

   for(int i=0;i<t2->P_dim_true[0];i++)
      for(int j=0;j<t2->P_dim_true[1];j++)
        t1->P[i][j] = t2->P[i][j];
   //copy rowsum
   memcpy(t1->rowsum,t2->rowsum,t2->P_dim_true[0]*sizeof(int));   
   //copy colsum
   memcpy(t1->colsum,t2->colsum,t2->P_dim_true[1]*sizeof(int));   
}

//create empty table.
TABLE * table(int dimT_1, int dimT_2, int dimP_1, int dimP_2)
{
   //cout<<"               TABLE         MALLOC\n"; 
   //printf("Creating table %d %d , %d %d\n",dimT_1,dimT_2,dimP_1,dimP_2);
   TABLE * Tab = (TABLE*)malloc(sizeof(TABLE));
   //copy T
   Tab->dim_T[0] = Tab->T_dim_true[0] = dimT_1;
   Tab->dim_T[1] = Tab->T_dim_true[1] = dimT_2;

   Tab->T = (int**)malloc(Tab->dim_T[0]*sizeof(int*));
   for(int i=0;i<Tab->dim_T[0];i++)
   {
      Tab->T[i] = (int*)malloc(Tab->dim_T[1]*sizeof(int));
      for(int j=0;j<Tab->dim_T[1];j++)
        Tab->T[i][j] = -1;
   }
   //copy P
   Tab->dim_P[0] = Tab->P_dim_true[0] = dimP_1;
   Tab->dim_P[1] = Tab->P_dim_true[1] = dimP_2;

   Tab->P = (int**)malloc(Tab->dim_P[0]*sizeof(int*));
   for(int i=0;i<Tab->dim_P[0];i++)
   {
      Tab->P[i] = (int*)malloc(Tab->dim_P[1]*sizeof(int));
      for(int j=0;j<Tab->dim_P[1];j++)
        Tab->P[i][j] = -1;
   }
   //copy rowsum
   Tab->rowsum = (int*)malloc(Tab->dim_P[0]*sizeof(int));
   for(int j=0;j<Tab->dim_P[0];j++)
       Tab->rowsum[j] = 0;

   //copy colsum
   Tab->colsum = (int*)malloc(Tab->dim_P[1]*sizeof(int));
   for(int j=0;j<Tab->dim_P[1];j++)
       Tab->colsum[j] = 0;
   return Tab;
}

void print(TABLE * Tab)
{
  printf("\tTable->\nT(%d %d) = \t P(%d %d) = \n----------\n",Tab->T_dim_true[0],Tab->T_dim_true[1],Tab->P_dim_true[0],Tab->P_dim_true[1]);
  printf("\tTable->\nT(%d %d) = \t P(%d %d) = \n----------\n",Tab->dim_T[0],Tab->dim_T[1],Tab->dim_P[0],Tab->dim_P[1]);

  for(int i=0;i<Tab->dim_P[0];i++)
  {
     for(int j=0;j<Tab->dim_T[1];j++)
     {
        cout<<Tab->T[i][j];
     }
     cout<<" -> ";
     for(int j=0;j<Tab->dim_P[1];j++)
     {
        printf("%3d\t",Tab->P[i][j]);
     }
     printf("=%d\n",Tab->rowsum[i]);
  }
  printf("\t");
  for(int j=0;j<Tab->dim_P[1];j++)
     printf("=%3d\t",Tab->colsum[j]);
  cout<<endl;
}

void free_table(TABLE * T)
{
   for(int i=0;i<T->T_dim_true[0];i++)
      free(T->T[i]);
   free(T->T);

   for(int i=0;i<T->P_dim_true[0];i++)
      free(T->P[i]);
   free(T->P);

   free(T->rowsum);  
   free(T->colsum);
   free(T);  
}

//create table when <= than two variables and binary phenotype present.
TABLE * createTable1(SNP_QUEUE * snp_queue,COMBINATION * C,PHENOTYPE * pheno)
{
   //cout<<"createTable1 called\n";
   SNP_type * snp1 = C->snps[0];
   SNP_type * snp2 = NULL;
   if(C->len==2)
      snp2 = C->snps[1];
   else if(C->len > 2)
   {
     cout<<"Error : table cannot be created using boolean operations for combination of length = "<<C->len<<endl;
     print(C);
     exit(1);
   }
   if(pheno->len>2)
   {
     cout<<"Error : table cannot be created using boolean operations phenotype having %d values"<<pheno->len<<endl;
     exit(1);
   }
   TABLE * Tab = NULL;

   if(C->table!=NULL)
   {
      Tab = C->table;
      Tab->dim_T[0] = 9;
      Tab->dim_T[1] = 2;
      Tab->dim_P[0] = 9;
      Tab->dim_P[1] = pheno->len;
   }
   else
   {
     if(C->len==2)
       Tab = table(9,2,9,pheno->len);
     else
       Tab = table(3,1,3,pheno->len);
   }

   Tab->colsum[0] = Tab->colsum[1] = 0;

   if(C->len==2) {
   int rsum = 0;
   int r = 0;
   Tab->P[r][0] = AND_count_3(snp1->C0,snp2->C0,pheno->C0);
   Tab->P[r][1] = AND_count_3(snp1->C0,snp2->C0,pheno->C1);
   rsum = Tab->P[r][0] + Tab->P[r][1];
   if(rsum>0)
   {
     Tab->T[r][0] = 0; 
     Tab->T[r][1] = 0;
     Tab->rowsum[r] = rsum; 
     Tab->colsum[0] += Tab->P[r][0];
     Tab->colsum[1] += Tab->P[r][1];
     r++;
   }
   //printf("00 -> %d\t%d\n",Tab->P[0][0],Tab->P[0][1]);
   
   Tab->P[r][0] = AND_count_3(snp1->C0,snp2->C1,pheno->C0);
   Tab->P[r][1] = AND_count_3(snp1->C0,snp2->C1,pheno->C1);
   rsum = Tab->P[r][0] + Tab->P[r][1];
   if(rsum>0)
   {
     Tab->T[r][0] = 0; 
     Tab->T[r][1] = 1;
     Tab->rowsum[r] = rsum; 
     Tab->colsum[0] += Tab->P[r][0];
     Tab->colsum[1] += Tab->P[r][1];
     r++;
   }
   //printf("01 -> %d\t%d\n",Tab->P[1][0],Tab->P[1][1]);

   Tab->P[r][0] = AND_count_3(snp1->C0,snp2->C2,pheno->C0);
   Tab->P[r][1] = AND_count_3(snp1->C0,snp2->C2,pheno->C1);
   rsum = Tab->P[r][0] + Tab->P[r][1];
   if(rsum>0)
   {
     Tab->T[r][0] = 0; 
     Tab->T[r][1] = 2;
     Tab->rowsum[r] = rsum; 
     Tab->colsum[0] += Tab->P[r][0];
     Tab->colsum[1] += Tab->P[r][1];
     r++;
   }
   //printf("02 -> %d\t%d\n",Tab->P[2][0],Tab->P[2][1]);

   Tab->P[r][0] = AND_count_3(snp1->C1,snp2->C0,pheno->C0);
   Tab->P[r][1] = AND_count_3(snp1->C1,snp2->C0,pheno->C1);
   rsum = Tab->P[r][0] + Tab->P[r][1];
   if(rsum>0)
   {
     Tab->T[r][0] = 1; 
     Tab->T[r][1] = 0;
     Tab->rowsum[r] = rsum; 
     Tab->colsum[0] += Tab->P[r][0];
     Tab->colsum[1] += Tab->P[r][1];
     r++;
   }
   //printf("10 -> %d\t%d\n",Tab->P[3][0],Tab->P[3][1]);

   Tab->P[r][0] = AND_count_3(snp1->C1,snp2->C1,pheno->C0);
   Tab->P[r][1] = AND_count_3(snp1->C1,snp2->C1,pheno->C1);
   rsum = Tab->P[r][0] + Tab->P[r][1];
   if(rsum>0)
   {
     Tab->T[r][0] = 1; 
     Tab->T[r][1] = 1;
     Tab->rowsum[r] = rsum; 
     Tab->colsum[0] += Tab->P[r][0];
     Tab->colsum[1] += Tab->P[r][1];
     r++;
   }
   //printf("11 -> %d\t%d\n",Tab->P[4][0],Tab->P[4][1]);

   Tab->P[r][0] = AND_count_3(snp1->C1,snp2->C2,pheno->C0);
   Tab->P[r][1] = AND_count_3(snp1->C1,snp2->C2,pheno->C1);
   rsum = Tab->P[r][0] + Tab->P[r][1];
   if(rsum>0)
   {
     Tab->T[r][0] = 1; 
     Tab->T[r][1] = 2;
     Tab->rowsum[r] = rsum; 
     Tab->colsum[0] += Tab->P[r][0];
     Tab->colsum[1] += Tab->P[r][1];
     r++;
   }
   //printf("12 -> %d\t%d\n",Tab->P[5][0],Tab->P[5][1]);

   Tab->P[r][0] = AND_count_3(snp1->C2,snp2->C0,pheno->C0);
   Tab->P[r][1] = AND_count_3(snp1->C2,snp2->C0,pheno->C1);
   rsum = Tab->P[r][0] + Tab->P[r][1];
   if(rsum>0)
   {
     Tab->T[r][0] = 2; 
     Tab->T[r][1] = 0;
     Tab->rowsum[r] = rsum; 
     Tab->colsum[0] += Tab->P[r][0];
     Tab->colsum[1] += Tab->P[r][1];
     r++;
   }
   //printf("20 -> %d\t%d\n",Tab->P[6][0],Tab->P[6][1]);

   Tab->P[r][0] = AND_count_3(snp1->C2,snp2->C1,pheno->C0);
   Tab->P[r][1] = AND_count_3(snp1->C2,snp2->C1,pheno->C1);
   rsum = Tab->P[r][0] + Tab->P[r][1];
   if(rsum>0)
   {
     Tab->T[r][0] = 2; 
     Tab->T[r][1] = 1;
     Tab->rowsum[r] = rsum; 
     Tab->colsum[0] += Tab->P[r][0];
     Tab->colsum[1] += Tab->P[r][1];
     r++;
   }
   //printf("21 -> %d\t%d\n",Tab->P[7][0],Tab->P[7][1]);

   Tab->P[r][0] = AND_count_3(snp1->C2,snp2->C2,pheno->C0);
   Tab->P[r][1] = AND_count_3(snp1->C2,snp2->C2,pheno->C1);
   rsum = Tab->P[r][0] + Tab->P[r][1];
   if(rsum>0)
   {
     Tab->T[r][0] = 2; 
     Tab->T[r][1] = 2;
     Tab->rowsum[r] = rsum; 
     Tab->colsum[0] += Tab->P[r][0];
     Tab->colsum[1] += Tab->P[r][1];
     r++;
   }
   Tab->dim_T[0] = Tab->dim_P[0] = r;
   //printf("22 -> %d\t%d\n",Tab->P[8][0],Tab->P[8][1]);
   }
   else
   {
   int rsum = 0;
   int r = 0;
   Tab->P[r][0] = AND_count_2(snp1->C0,pheno->C0);
   Tab->P[r][1] = AND_count_2(snp1->C0,pheno->C1);
   rsum = Tab->P[r][0] + Tab->P[r][1];
   if(rsum>0)
   {
     Tab->T[r][0] = 0; 
     Tab->rowsum[r] = rsum; 
     Tab->colsum[0] += Tab->P[r][0];
     Tab->colsum[1] += Tab->P[r][1];
     r++;
   }
   
   Tab->P[r][0] = AND_count_2(snp1->C1,pheno->C0);
   Tab->P[r][1] = AND_count_2(snp1->C1,pheno->C1);
   rsum = Tab->P[r][0] + Tab->P[r][1];
   if(rsum>0)
   {
     Tab->T[r][0] = 1;
     Tab->rowsum[r] = rsum; 
     Tab->colsum[0] += Tab->P[r][0];
     Tab->colsum[1] += Tab->P[r][1];
     r++;
   }

   Tab->P[r][0] = AND_count_2(snp1->C2,pheno->C0);
   Tab->P[r][1] = AND_count_2(snp1->C2,pheno->C1);
   rsum = Tab->P[r][0] + Tab->P[r][1];
   if(rsum>0)
   {
     Tab->T[r][0] = 2;
     Tab->rowsum[r] = rsum; 
     Tab->colsum[0] += Tab->P[r][0];
     Tab->colsum[1] += Tab->P[r][1];
     r++;
   }
   Tab->dim_T[0] = Tab->dim_P[0] = r;
   }
   //print(Tab);
   C->table = Tab;
   return Tab; 
}

//create table when more than two variables and phenotype present.
TABLE * createTable2(SNP_QUEUE * snp_queue,COMBINATION * C,PHENOTYPE * pheno)
{
   //cout<<"createTable2 called\n";
   int nsample = pheno->nsample;
   int offset = (nsample % CHUNK_SZ); //location of bit corresponding to the last sample within the last chunk.
   int initial_shifts = CHUNK_SZ - offset; //only for the last chunk which may be partially full from the left.

   //printf("nsample = %d offset = %d\n",nsample,offset); 
   //printf("n_chunk = %d\n",n_chunk);
   //printf("initial_shifts = %d\n",initial_shifts);

   tr1::unordered_map< string, vector<int> > H;
   tr1::unordered_map< string, vector<int> >::iterator it;
   
   int pheno_index = CHUNK_SZ-1;
   for(int i=0;i<n_chunk;i++)
   {
      //printf("For chunk = %d ->\n",i);
      chunk_t C0[C->len];  
      chunk_t C1[C->len];
      //printf("(%d) gathering chunks...\n",i);  
      for(int j=0;j<C->len;j++)
      {
         //int id = C->snps[j]->id;
         //SNP_type * snp = nq_get(snp_queue,id);
         SNP_type * snp = C->snps[j];
         C0[j] = snp->C0[i];
         C1[j] = snp->C1[i];
         //printf("SNP = %d : C0 = %s\n",j,itob(C0[j]));
         //printf("SNP = %d : C1 = %s\n",j,itob(C1[j]));
      }

      chunk_t Z = (chunk_t)1;
      int start = CHUNK_SZ-1;
      if(i==n_chunk-1) //last chunk ?
      {
         Z = (chunk_t)Z<<initial_shifts;
         start = offset-1;
         pheno_index -= initial_shifts;
      }
      //printf("start = %d\n",start);
      char rowval[C->len+1];
      int idx = pheno_index;
      for(int j=start;j>=0;j--)
      {
         for(int k=0;k<C->len;k++)
         {
           if(C0[k] & Z) //0 for row i snp k ?
              rowval[k] = '0'; 
           else if(C1[k] & Z) //1 for row i snp k ?
              rowval[k] = '1';
           else  
              rowval[k] = '2';
         }
         rowval[C->len] = '\0';
         //printf("j=%d row=%s ",j,rowval);
         //printf("     pheno[%d] = %d\n",idx,pheno->data[idx]);
         int phenoval = pheno->data[idx];
         it = H.find(rowval);
         if(it==H.end())
         {
            //printf("Adding new row = %s\n",rowval);
            vector<int> vec(pheno->len,0) ;
            vec[phenoval] = 1;
            H[rowval] = vec;
         }
         else
         {
            vector<int> vec = (*it).second;
            (vec[phenoval])++; 
            H[rowval] = vec;
         }
         Z = (chunk_t)Z<<1;
         idx--;
         //cout<<"size="<<H.size()<<endl;
         //cout<<"Updated values for "<<rowval<<endl;
         //for(int r=0;r<pheno->len;r++)
         //   cout<<"["<<r<<"]="<<H[rowval][r]<<" ";
         //cout<<endl; 
      }
      pheno_index += CHUNK_SZ; 
   }
  
   TABLE * Tab;
   if(C->table==NULL)
      Tab = table(H.size(),C->len,H.size(),pheno->len);
   else
   {
      Tab = C->table;
      Tab->dim_T[0] = H.size();
      Tab->dim_T[1] = C->len;
      Tab->dim_P[0] = H.size();
      Tab->dim_P[1] = pheno->len;
      //cout<<"Already allocated : "<<Tab->T_dim_true[0]<<","<<Tab->T_dim_true[1]<<endl;
      //cout<<"Already allocated : "<<Tab->P_dim_true[0]<<","<<Tab->P_dim_true[1]<<endl;
      //cout<<"Resized : "<<Tab->dim_T[0]<<","<<Tab->dim_T[1]<<endl;
      //cout<<"Resized : "<<Tab->dim_P[0]<<","<<Tab->dim_P[1]<<endl;

      for(int i=0;i<Tab->P_dim_true[0];i++)
        Tab->rowsum[i] = 0;
      for(int i=0;i<Tab->P_dim_true[1];i++)
        Tab->colsum[i] = 0;
   }

   int r = 0;

   for(it=H.begin();it!=H.end();it++)
   {
      string key = (*it).first;
      vector<int> v = (*it).second;

      const char* s = key.c_str();
      for(unsigned int c=0;c<key.size();c++)
      {
         Tab->T[r][c] = s[c] - '0';
      }
      for(int c=0;c<pheno->len;c++)
      {
         Tab->P[r][c] = v[c];
         Tab->rowsum[r] += Tab->P[r][c];
         Tab->colsum[c] += Tab->P[r][c];
      }
      r++;
   }
   //print(Tab); 
   C->table = Tab;
   return Tab; 
}

void marginalise(TABLE * Tab, int Clen,vector<int> cols, PHENOTYPE * pheno, bool marginalise_phenotype, TABLE * newTab)
{
   //cout<<"marginalizing.....[";
   //for(int i=0;i<cols.size();i++) cout<<cols[i]<<" ";
   //cout<<"]"<<endl;

   //the last value on col can be the phenotype. this is not needed. marginalise_phenotype = false when the last col is phenotype.
   //marginalise_phenotype = true when no phenotype is present in col.
   //remove phenotype from col if present.

   int last = cols.back();
   if(last>=Clen)
   {
      cols.pop_back();
   }

   //marginalise contingency table Tab using only the cols. Sum over phenotype values only if marginalise_phenotype = true. 
   int N = Tab->dim_T[0];
   tr1::unordered_map< string, vector<int> > H;
   tr1::unordered_map< string, vector<int> >::iterator it;
   //cout<<N<<" "<<cols.size()<<endl; 
   for(int i=0;i<N;i++)
   {
      char rowval[cols.size()+1];
      unsigned int j = 0;
      for(j=0;j<cols.size();j++)
      {
         int k = cols[j];
         //cout<<k<<" ";
         if(Tab->T[i][k]==0) 
            rowval[j] = '0'; 
         else if(Tab->T[i][k]==1) 
            rowval[j] = '1';
         else  
            rowval[j] = '2';
      }
      rowval[j] = '\0';
      //cout<<"rowval="<<rowval<<endl;
      it = H.find(rowval);
      if(it==H.end())
      {
         if(marginalise_phenotype==false)
         {
           vector<int> vec(pheno->len,0);
           for(j=0;j<pheno->len;j++)
              vec[j] += Tab->P[i][j]; 
           H[rowval] = vec;
         }
         else
         {
           vector<int> vec(1,0);
           for(j=0;j<pheno->len;j++)
              vec[0] += Tab->P[i][j]; 
           H[rowval] = vec;
         }
      }
      else
      {
         vector<int> vec = (*it).second;
         if(marginalise_phenotype==false)
         {
           for(j=0;j<pheno->len;j++)
             vec[j] += Tab->P[i][j]; 
         }
         else
         {
           for(j=0;j<pheno->len;j++)
             vec[0] += Tab->P[i][j]; 
         }
         H[rowval] = vec;
      }
   }
 
   if(newTab == NULL)
   {
      if(marginalise_phenotype==false)
         newTab = table(H.size(),cols.size(),H.size(),pheno->len);
      else
         newTab = table(H.size(),cols.size(),H.size(),1);
   }
   else
   {
      if(newTab->T_dim_true[0] < H.size() || newTab->T_dim_true[1] < cols.size() || newTab->P_dim_true[1] < pheno->len)
      {
         printf("-Error in marginalise : provided table not big enough !\n");
         printf("Provided size = T(%d %d) P(%d %d) \n",newTab->T_dim_true[0],newTab->T_dim_true[1],newTab->P_dim_true[0],newTab->P_dim_true[1]);
         printf("Required size = T(%d %d) P(%d %d)\n",H.size(),cols.size(),H.size(),pheno->len);
         exit(1);
      }
      newTab->dim_T[0] = newTab->dim_P[0] = H.size();
      newTab->dim_T[1] = cols.size(); 
      if(marginalise_phenotype==false)
         newTab->dim_P[1] = pheno->len;
      else
         newTab->dim_P[1] = 1;

      for(int i=0;i<newTab->dim_P[0];i++)
          newTab->rowsum[i] = 0;
      for(int i=0;i<newTab->dim_P[1];i++)
          newTab->colsum[i] = 0;
   }

   int r = 0;
   for(it=H.begin();it!=H.end();it++)
   {
      string key = (*it).first;
      vector<int> v = (*it).second;

      const char* s = key.c_str();
      for(unsigned int c=0;c<key.size();c++)
      {
         newTab->T[r][c] = s[c] - '0';
      }
      if(marginalise_phenotype==false)
      {
        for(int c=0;c<pheno->len;c++)
        {
           newTab->P[r][c] = v[c];
           newTab->rowsum[r] += newTab->P[r][c];
           newTab->colsum[c] += newTab->P[r][c];
        }
      }
      else
      {
         newTab->P[r][0] = v[0];
         newTab->rowsum[r] += newTab->P[r][0];
         newTab->colsum[0] += newTab->P[r][0];
      }
      r++;
   }
   //print(newTab);
   //printf("oooooooooooooooooooooooooooooooooooooo\n"); 
}

/*
void permute_table(TABLE * tab)
{
  int key = 0;
  int seed = 123456789;
  TABLE * permutedTab = table(tab);//copy of tab.
  for(int i=0;i<N_PERM;i++)
  {
    run_ASA159(tab->rowsum, tab->colsum, &key, permutedTab, &seed);
    run_ASA159(tab->rowsum, tab->colsum, &key, permutedTab, &seed);
    run_ASA159(tab->rowsum, tab->colsum, &key, permutedTab, &seed);
  }
}
*/

void run_ASA159(int *rowsum, int *colsum, int *key, TABLE * permuted_tab, int * seed)
{
    int nrows = permuted_tab->dim_P[0];
    int ncols = permuted_tab->dim_P[1];
    int* matrix = new int[nrows*ncols];

    int err = 0;
    rcont2 (nrows,ncols, rowsum, colsum,key,seed,matrix, &err);
    
    //copy matrix to P and update rowsum and colsum.
    for(int j=0;j<permuted_tab->dim_P[1];j++)
    {
       for(int i=0;i<permuted_tab->dim_P[0];i++)
       {
          int index = j*nrows+i;
          permuted_tab->P[i][j] = matrix[index];
       }
    }
    //print(permuted_tab);
}
 
