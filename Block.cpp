#include <stdio.h>
#include <iostream>

int main()
{
   int block[5] = {1,1,1,2,2};
   int pair[5][5] = { {0,1,2,3,4}, {1,0,12,13,14}, {2,12,0,23,24}, {3,13,23,0,34},{4,14,24,34,0}};
   
   for(int snp1=0;snp1<5;snp1++)
   {
      int curr_block = block[snp1+1];
      char best_pair[20];
      int best = -1;
      sprintf(best_pair,"");
      for(int snp2=snp1+1;snp2<5;snp2++)
      {
        printf("s1=%d  s2=%d val=%d block = %d\n",snp1,snp2,pair[snp1][snp2],curr_block);
        int val = pair[snp1][snp2];
        if(curr_block==block[snp2])
        {
           if(val >= best)
           {
              sprintf(best_pair,"%d - %d = %d",snp1,snp2,val);
              best = val;
              printf("      update : %d - %d = %d\n",snp1,snp2,val);
           }  
        }
        else
        {
           printf("* %s\n\n",best_pair);
           best = val;
           sprintf(best_pair,"%d - %d = %d",snp1,snp2,val);
           printf("      update : %d - %d = %d\n",snp1,snp2,val);
           curr_block = block[snp2];
        }
      }
      printf("* %s\n\n",best_pair);
   }  
 
   return 0;
}
