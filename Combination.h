#ifndef _COMBINATION_H
#define _COMBINATION_H

#include "FastEpi.h"
#include "Table.h"

COMBINATION * combination();
COMBINATION * combination(COMBINATION * C);
void print(COMBINATION * C);
void append(COMBINATION * C, SNP_type * u);
void free_combination(COMBINATION * C);
void combination_overwrite(COMBINATION * C1,COMBINATION * C2);
void remove(COMBINATION * C, SNP_type * u);
void make_key(COMBINATION * C);
void print_key(COMBINATION * C);
#endif
