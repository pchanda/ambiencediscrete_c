#ifndef _COMPRESS_H
#define _COMPRESS_H

#include "FastEpi.h"

char *itob(chunk_t x);
chunk_t compress(int* x,int n);
void compress_snp(int* x,SNP_type* XB);
void print(SNP_type* XB);
void compress_pheno(PHENOTYPE* P);
#endif
