#ifndef _TABLE_H
#define _TABLE_H

#include "FastEpi.h"
#include "Compress.h"
#include "SQueue.h"
#include "asa159.h"
#include "Combination.h"

TABLE * table(TABLE * t);
void table(TABLE * t1, TABLE * t2);
TABLE * table(int, int,int,int);
TABLE * createTable1(SNP_QUEUE * snp_queue,COMBINATION * C,PHENOTYPE * pheno);
TABLE * createTable2(SNP_QUEUE * snp_queue,COMBINATION * C,PHENOTYPE * pheno);
void marginalise(TABLE * Tab, int Clen, vector<int> cols, PHENOTYPE * pheno, bool, TABLE * newTab);
void run_ASA159(int *rowsum, int *colsum, int *key, TABLE * permuted_tab, int * seed);
void permute_table(TABLE * tab);
void free_table(TABLE * table);
void print(TABLE * Tab);
#endif
