#ifndef FASTEPI_H_
#define FASTEPI_H_

#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <time.h>
#include <limits.h>
#include <string>
#include <vector>
#include <tr1/unordered_map>
#include <tr1/unordered_set>
#include <stdint.h>
#include <iostream>
#include <sstream>
#include <iterator>
#include <queue>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_cdf.h>

#define CHUNK_SZ LONG_BIT
#define SNP_ID_LEN 20

#define MAX_FILENAME_LEN 100
#define MAX_INCLUDED_SNP 20 
#define MAX_N_INDIV 20000
#define MAX_LINE_WIDTH 120000
#define PHENOTYPE_VALUE_LEN 20
#define INDIV_ID_LEN 20
#define MAX_COMB_SIZE 16
#define MAX_COMB_CHARS 200
#define MAX_PHEN_VALUE 10
#define GENE_ID_LEN 200
#define MAX_GENES_PER_SNP 100
#define BLOCK_CUTOFF 0.8
#define FILTER_BY_BLOCK true

extern bool VERBOSE;
extern double PAI_PVAL_CUTOFF;
extern int GENE_FLANK;
extern bool SHOW_NEG_KWII;
extern int MAX_BUFFER_SIZE;
extern int MAX_ITER;
extern int N_PERM;
extern int SEED;
extern bool PAIRWISE_START;

using namespace std;

typedef unsigned long chunk_t;

typedef struct 
{
  char * tped_file;
  char * genes_file;
  char * pheno_file;
  char * out_file;
  int buffer_size;
  int max_iter;
  int flank;
  double pai_pval_cutoff;
  bool show_neg_kwii;
  int nperm;
} PAR;  

typedef struct GN
{
  char name[GENE_ID_LEN];
  int gene_id; //unique id.
  int chr;
  int bp_start;
  int bp_end;
  int nSNP; // total number of SNPs in a gene
  int snp_start; //coordinate for the first SNP in this gene in the circular queue
  int snp_end; //coordinate for the last SNP in this gene in the circular queue
  bool skip;
  struct GN * next; //pointer to the next gene on this chromosome.
} GENE_type;

typedef struct BL
{
   int id;
   int block_id;
   int chr;
   int snp_start;
   int snp_end;
   struct BL * next;
} Block_type;

typedef struct SNP_GENES
{
  GENE_type * gene;
  struct SNP_GENES * next;
} SNP_GENES_type;

typedef struct SNP
{
  int id;
  int* geno; //orignal data, only used for testing.
  //binary format data
  chunk_t* C0;
  chunk_t* C1;
  chunk_t* C2;

  //SNP information
  char name[SNP_ID_LEN];
  int chr;
  double MAF;
  int bp;
  int nsample;
  int LD_block_id;
  int nGene;
  SNP_GENES_type * snp_genes;
  SNP_GENES_type * tail;
} SNP_type; //binary

typedef struct SNP_QUEUE_TYPE
{
  SNP_type ** SNPS;
  SNP_type * head;
  SNP_type * tail;
  int size;
} SNP_QUEUE;

typedef struct
{
  int * data; 
  chunk_t* C0; //will be used only if the phenotype is binary
  chunk_t* C1; //will be used only if the phenotype is binary
  int nsample; //number of samples.
  int values[MAX_PHEN_VALUE]; //distinct values.
  int len; //count of distinct values.
  double entropy;
} PHENOTYPE;

typedef struct
{
  //one extra row and column to be there to hold rowsums and colsums.
  int ** T;
  int ** P;
  int dim_T[2];//to store dimensions of T
  int dim_P[2];//to store dimensions of P
  int * rowsum; //sum of each row across columns.
  int * colsum; //sum of each column across rows.

  int T_dim_true[2];//to store true dimensions of T, the actual malloced memory
  int P_dim_true[2];//to store true dimensions of P, the actual malloced memory
} TABLE;

typedef struct
{
  string * key;
  int64_t hashid;
  int * comb_str;
  double kwii;
  double pai;
  double entropy1; //HX
  double entropy2; //HXP
  SNP_type ** snps;//array of pointers to snps
  TABLE * table;
  int len;
  int parents[MAX_COMB_SIZE]; //index of parent combination.
  int parents_len;
  double pai_pvalue; //pai pvalue.
  double kwii_pvalue; //kwii pvalue.
  int last_snp_index; //index of the last snp to enter this combination
  int last_but_one_snp_index; //index of the last but one snp to enter this combination
} COMBINATION;

typedef struct
{
  double kwii;
  int perms;
  int hits;
} KWII_PERM;

const chunk_t m1  = 0x5555555555555555; //binary: 0101...
const chunk_t m2  = 0x3333333333333333; //binary: 00110011..
const chunk_t m4  = 0x0f0f0f0f0f0f0f0f; //binary:  4 zeros,  4 ones ...
const chunk_t m8  = 0x00ff00ff00ff00ff; //binary:  8 zeros,  8 ones ...
const chunk_t m16 = 0x0000ffff0000ffff; //binary: 16 zeros, 16 ones ...
const chunk_t m32 = 0x00000000ffffffff; //binary: 32 zeros, 32 ones
const chunk_t hff = 0xffffffffffffffff; //binary: all ones
const chunk_t h01 = 0x0101010101010101; //the sum of 256 to the power of 0,1,2,3...

extern int n_chunk;
extern bool gene_based;
#endif //FASTEPI_H_
