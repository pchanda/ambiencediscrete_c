CC=g++
CFLAGS=-g -Wall
LFLAGS= -lgsl -lgslcblas 

all: Ambience

Ambience:  Ambience.o Queue.o IO.o Compress.o Combination.o asa159.o Table.o Stat.o
		$(CC)  -Wall -o Ambience Ambience.o Queue.o IO.o Compress.o Combination.o asa159.o Table.o Stat.o $(LFLAGS) 
		rm *.o
Ambience.o:
	$(CC) $(CFLAGS) -c Ambience.cpp -o Ambience.o
Queue.o:
	$(CC) $(CFLAGS) -c Queue.cpp -o Queue.o
IO.o:
	$(CC) $(CFLAGS) -c IO.cpp -o IO.o
Compress.o:
	$(CC) $(CFLAGS) -c Compress.cpp -o Compress.o
Combination.o:
	$(CC) $(CFLAGS) -c Combination.cpp -o Combination.o
asa159.o:
	$(CC) $(CFLAGS) -c asa159.c -o asa159.o
Table.o:
	$(CC) $(CFLAGS) -c Table.cpp -o Table.o
Stat.o:
	$(CC) $(CFLAGS) -c Stat.cpp -o Stat.o

clean:
	rm Ambience
