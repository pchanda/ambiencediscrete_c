#include "Stat.h"

bool gene_based;

int64_t get_hash(const char* s)
{
    int64_t hash = 0;
    int c;

    while((c = *s++))
    {
        /* hash = hash * 33 ^ c */
        hash = ((hash << 5) + hash) ^ c;
    }
    if(hash < 0)
    {
       printf("Error : hash overflow for %s\n",s);
       exit(0);
    }
    return hash;
}

double pai(COMBINATION * C,double Hpheno,int nsample)
{
   TABLE * tab = C->table;
   int r = tab->dim_P[0];
   int c = tab->dim_P[1];
   double HX =0;
   double HXY = 0;
   for(int i=0;i<r;i++)
   { 
      for(int j=0;j<c;j++)
      { 
         double pxy = ((double)tab->P[i][j]/nsample);
         if(pxy>0)
            HXY += -pxy * log(pxy);
      }
      double px = (double)tab->rowsum[i]/nsample;
      if(px>0)
         HX += -px * log(px);
   }
   //cout<<HX<<" "<<Hpheno<<" "<<HXY<<endl;
   double pai = HX + Hpheno - HXY;
   C->pai = pai;
   C->entropy1 = HX;
   C->entropy2 = HXY;
   return pai;
}

double entropy(TABLE * T,int nsample)
{
   int r = T->dim_P[0];
   int c = T->dim_P[1];
   double H =0;
   for(int i=0;i<r;i++)
   { 
      for(int j=0;j<c;j++)
      { 
         double p = ((double)T->P[i][j]/nsample);
         if(p>0)
            H += -p * log(p);
      }
   }
   return H;
}

double sq_correlation(int * v1, int *v2, int n)
{ 
    double sum1 = 0;
    double sum2 = 0;
    double sum11 = 0;
    double sum12 = 0;
    double sum22 = 0;
    double k = 0;
    for(int i=0; i<n; i++)
    {
        if(v1[i]>=0 && v2[i]>=0)
        {
          sum12 += v1[i]*v2[i];
          sum1 +=v1[i];
          sum2 +=v2[i];
          sum11 +=v1[i]*v1[i];
          sum22 +=v2[i]*v2[i];
          k++;
        }
    }
    double c = (sum12/k - sum1/k*sum2/k)/sqrt((sum11/k-sum1/k*sum1/k)*(sum22/k-sum2/k*sum2/k));
    return pow(c,2.0);
}

void single(SNP_QUEUE* snp_queue, PHENOTYPE * phenotype, vector<COMBINATION*> * BUFFER)
{
   //Do not alloc and free combination tables again and again.
   //Better create tables and use it for all iterations.
   TABLE * T = table(3, 2, 3, phenotype->len);//malloc
   TABLE * bestT = table(3, 2, 3, phenotype->len);//malloc

   COMBINATION * C = combination();   //malloc
   COMBINATION * bestC = combination(); //malloc
   C->table = T;
   bestC->table = bestT;

   double scale = 1.0/(phenotype->nsample * log(2.0));

   int curr_block = -1;
   int curr_chr = -1;
   double bestpai_block = -1;
   bool updated = false;

   for(int i=0;i<snp_queue->size;i++)
   {
      SNP_type * snp = (SNP_type*)nq_get(snp_queue,i);

      if(gene_based)
      {
         if(snp->nGene==0)
            continue;
      }

      if(curr_block < 0)
      {
         curr_chr = snp->chr;
         curr_block = snp->LD_block_id;
      }
      append(C,snp);
      if(phenotype->len <=2)
         createTable1(snp_queue,C,phenotype); //should NOT malloc
      else
         createTable2(snp_queue,C,phenotype); //should NOT malloc
      double pai_value = pai(C, phenotype->entropy, phenotype->nsample);
      double df = (C->table->dim_P[0]-1)*(C->table->dim_P[1]-1);
      C->pai_pvalue = gsl_cdf_gamma_Q(C->pai, df/2.0, scale);
      //printf("Computed ->");print(C);

      if(snp->LD_block_id < 0) //no block information
      {
         if(bestC->pai>0)
         {
            COMBINATION * Ccopy = combination(bestC);//malloc to store in BUFFER.
            BUFFER->push_back(Ccopy);
            bestC->pai = 0;
            bestC->pai_pvalue = 1.0;
            //printf("\tStored 3 -> ");print(Ccopy);
         }
         if(C->pai_pvalue < PAI_PVAL_CUTOFF)
         {
            COMBINATION * Ccopy = combination(C);//malloc to store in BUFFER.
            printf("block = -1, stored "); print(C);
            BUFFER->push_back(Ccopy);
         }
      }
      else if(curr_block==snp->LD_block_id && curr_chr==snp->chr)
      {
        if(C->pai_pvalue < PAI_PVAL_CUTOFF)
        {
          if(pai_value >= bestpai_block)
          {
            combination_overwrite(bestC,C); //should not malloc
            bestpai_block = pai_value;
            //printf("Updated 1 -> ");print(bestC);
            updated = true;
          }
        }
      }
      else
      {
          if(bestC->pai>0 && updated)
          {
            COMBINATION * Ccopy = combination(bestC);//malloc to store in BUFFER.
            //printf("\tStored 1 -> ");print(Ccopy);
            BUFFER->push_back(Ccopy);
          }

          updated = false;
          bestC->pai = 0;
          bestC->pai_pvalue = 1.0;

          if(C->pai_pvalue < PAI_PVAL_CUTOFF)
          {
             combination_overwrite(bestC,C); //should not malloc
             bestpai_block = pai_value;
             //printf("Updated 2 -> ");print(bestC);
             curr_block = snp->LD_block_id;
             curr_chr = snp->chr;
             updated = true;
          }
      }
      remove(C,snp);
      //printf("--------------------\n");
   }

   if(bestC->pai_pvalue < PAI_PVAL_CUTOFF && updated==true)
   {
     COMBINATION * Ccopy = combination(bestC);
     //printf("\tStored 2 -> ");print(Ccopy);
     BUFFER->push_back(Ccopy);
   }

   free_combination(C);
   free_combination(bestC);

   printf(">>>>>>>>>>>>>>>>>>>>>>>>>>\n");
   for(int i=0;i<BUFFER->size();i++)
   {
      COMBINATION * C = (*BUFFER)[i];
      print(C);
   }
}

void pairwise_block(SNP_QUEUE* snp_queue, PHENOTYPE * phenotype, vector<COMBINATION*> * BUFFER,Block_type * startBlock)
{
   if(!startBlock)
   {
      printf("-Error, cannot run pairwise without block information\n");
      exit(1);
   }
   printf("-Pairwise PAI computation by snp LD blocks...\n");

   TABLE * T = table(9, 2, 9, phenotype->len);//malloc
   TABLE * bestT = table(9, 2, 9, phenotype->len);//malloc

   COMBINATION * C = combination();   //malloc
   COMBINATION * bestC = combination(); //malloc
   C->table = T;
   bestC->table = bestT;

   double scale = 1.0/(phenotype->nsample * log(2.0));

   Block_type * b1 = startBlock;   
   while(b1)
   {
      int start_1 = b1->snp_start;
      int end_1 = b1->snp_end;
      Block_type * b2 = b1->next;   
      while(b2)
      {
         double bestpai_block = -1;
         bool updated = false;      
         for(int i=start_1;i<=end_1;i++)
         {
            SNP_type * snp1 = (SNP_type*)nq_get(snp_queue,i);
            if(gene_based)
            {
               if(snp1->nGene==0)
                  continue;
            }
            append(C,snp1);
            int start_2 = b2->snp_start;
            int end_2 = b2->snp_end;
            for(int j=start_2;j<=end_2;j++)
            {
               SNP_type * snp2 = (SNP_type*)nq_get(snp_queue,j);
               if(gene_based)
               {
                  if(snp2->nGene==0)
                     continue;
               }
               append(C,snp2);
               if(phenotype->len <=2)
                  createTable1(snp_queue,C,phenotype); //should NOT malloc
               else
                  createTable2(snp_queue,C,phenotype); //should NOT malloc
               double pai_value = pai(C, phenotype->entropy, phenotype->nsample);
               //printf("Computed ->");print(C);print(C->table);
               if(pai_value > bestpai_block)
               {
                  double df = (C->table->dim_P[0]-1)*(C->table->dim_P[1]-1);
                  C->pai_pvalue = gsl_cdf_gamma_Q(C->pai, df/2.0, scale);
                  if(C->pai_pvalue < PAI_PVAL_CUTOFF)
                  {
                    combination_overwrite(bestC,C); //should not malloc
                    bestpai_block = pai_value;
                    //printf("Updated 1 -> ");print(bestC);
                    updated = true;
                  }
               }
               //printf("  pair %d %d \n",snp1->id,snp2->id);
               remove(C,snp2);
            }
            remove(C,snp1);
         }
         if(bestC->pai>0 && updated)
         {
            COMBINATION * Ccopy = combination(bestC); //malloc to store in BUFFER.
            //printf("\tStored 1 -> ");print(Ccopy);
            BUFFER->push_back(Ccopy);
         }
         b2 = b2->next;
      }
      b1 = b1->next;
   }

   free_combination(C);
   free_combination(bestC);

   printf("------------Significant 2-snp combinations by block-------------\n");
   for(int i=0;i<BUFFER->size();i++)
   {
      COMBINATION * c = (*BUFFER)[i];
      printf("BUFFER[%d] = ",i);print(c);
   }
}

void pairwise_all(SNP_QUEUE* snp_queue, PHENOTYPE * phenotype, vector<COMBINATION*> * BUFFER)
{
   //assuming no block information for each snp.
   //Do not alloc and free combination tables again and again.
   //Better create tables and use it for all iterations.
   TABLE * T = table(9, 2, 9, phenotype->len);//malloc
   COMBINATION * C = combination();   //malloc
   C->table = T;

   double scale = 1.0/(phenotype->nsample * log(2.0));

   for(int i=0;i<snp_queue->size;i++)
   {
      SNP_type * snp1 = (SNP_type*)nq_get(snp_queue,i);
      if(gene_based)
      {
         if(snp1->nGene==0)
            continue;
      }
      append(C,snp1);
      for(int j=i+1;j<snp_queue->size;j++)
      {
         //printf("[%d %d] updated = %d\n",i,j,updated);
         SNP_type * snp2 = (SNP_type*)nq_get(snp_queue,j);

         if(gene_based)
         {
            if(snp2->nGene==0)
              continue;
         }
         append(C,snp2);

         if(phenotype->len <=2)
           createTable1(snp_queue,C,phenotype); //should NOT malloc
         else
           createTable2(snp_queue,C,phenotype); //should NOT malloc
         double pai_value = pai(C, phenotype->entropy, phenotype->nsample);

         double df = (C->table->dim_P[0]-1)*(C->table->dim_P[1]-1);
         C->pai_pvalue = gsl_cdf_gamma_Q(C->pai, df/2.0, scale);
         printf("Computed ->");print(C);//print(C->table);

         if(C->pai_pvalue < PAI_PVAL_CUTOFF)
         {
             COMBINATION * Ccopy = combination(C);//malloc to store in BUFFER.
             printf("Stored "); print(C);
             BUFFER->push_back(Ccopy);
         }
         remove(C,snp2);
      }
      remove(C,snp1);
   }

   free_combination(C);

   printf(">>>>>>>>>>>>>>>>>>>>>>>>>>\n");
   for(int i=0;i<BUFFER->size();i++)
   {
      COMBINATION * c = (*BUFFER)[i];
      print(c);
   }
}

void iterate(SNP_QUEUE* snp_queue, PHENOTYPE * phenotype, vector<COMBINATION*> * BUFFER, int max_iter,int order, int str, FILE * fp_pai)
{
   printf("iterate %d times\n",max_iter);
   int start = str;
   for(int i=0;i<max_iter;i++)
   {
      cout<<"iter = "<<i<<endl;
      start = higher_order(snp_queue, phenotype, BUFFER, ++order, start);//should fill BUFFER_new.
      if(BUFFER->size()==0)
      {
          break;
      }
      printf(">>>>>>>>>>>>>>>>>>>>>>>>>>\n");
      //for(int i=start;i<BUFFER->size();i++)
      for(int i=start;i<BUFFER->size();i++)
      {
        COMBINATION * c = (*BUFFER)[i];
        printf("BUFFER[%d] = ",i);print(c);
      }
   }

   for(int i=0;i<BUFFER->size();i++)
   {
      COMBINATION * c = (*BUFFER)[i];
      make_key(c);
      fprintf(fp_pai,"%s\t%g\t%g\n",c->key->c_str(),c->pai,c->pai_pvalue); 
   }
}

bool redundancy(COMBINATION * C, SNP_type * snp)
{
  if(snp->LD_block_id < 0)
    return false;

  for(int i=0;i<C->len;i++)
  {
     if(C->snps[i]->chr == snp->chr && C->snps[i]->LD_block_id == snp->LD_block_id)
     {
        return true;
     }
  }
  return false;
}

bool operator <(COMBINATION x1, COMBINATION x2)
{
      return(x1.pai > x2.pai);
}

void push(priority_queue<COMBINATION *> * PQ, COMBINATION *C)
{
   if(PQ->size() >= MAX_BUFFER_SIZE)
      PQ->pop();
   PQ->push(C);
   //cout<<" Queued combination "<<*(C->key)<<" queue size = "<<PQ->size()<<endl;
} 

int higher_order(SNP_QUEUE* snp_queue, PHENOTYPE * phenotype, vector<COMBINATION*> * BUFFER, int order, int start)
{
   //start gives the position in BUFFER to start iterations.
   //printf("-------%d %d-------------------\n",start,order);
   //order = count of snps.
   int m = (int)pow(3.0,order);  
   TABLE * T = table(m, order, m, phenotype->len);//malloc
   TABLE * bestT = table(m, order, m, phenotype->len);//malloc
   
   COMBINATION * C = combination();   //malloc
   COMBINATION * bestC = combination(); //malloc
   C->table = T;
   bestC->table = bestT;

   //tr1::unordered_map<int64_t,string*> COMB_MAP;
   //tr1::unordered_map<int64_t,string*>::iterator it;

   tr1::unordered_set<string> COMB_SET;
   tr1::unordered_set<string>::iterator it;

   double scale = 1.0/(phenotype->nsample * log(2.0));
   int end = BUFFER->size();

   int64_t est_pairs = ((int64_t)snp_queue->size)*(end-start+1);
   int64_t gap = (int64_t)est_pairs/(1000.0);
   printf("Estimated combinations = %d x %llu = %llu gap = %llu\n",snp_queue->size,(end-start+1),est_pairs,gap);
   int64_t count = 0;
   int64_t count_tot = 0;
   priority_queue<COMBINATION *> * PQ = new priority_queue<COMBINATION *>();    

   for(int i=start;i<end;i++)
   {
      COMBINATION * Corg = (*BUFFER)[i];
      combination_overwrite(C,Corg); //should not malloc, will copy tables.
   
      if(VERBOSE)
      {
         printf("\nADDING to Combination :"); 
         print(C);
      }

      int curr_block = -1;
      int curr_chr = -1;
      double bestpai_block = -1;
      bool updated = false;  

      for(int j=0;j<snp_queue->size;j++)
      {
         SNP_type * snp2 = (SNP_type*)nq_get(snp_queue,j);

         if(gene_based)
         {
            if(snp2->nGene==0)
              continue;
         }
         
         count++;

         //check if a snp in similar chr and block is already in the combination.
         if(redundancy(C,snp2))
         {
            if(VERBOSE)
               printf("\tskipping snp = %s\n",snp2->name);
            continue;
         }
         append(C,snp2);
         make_key(C);
         //C->hashid = get_hash(C->key->c_str());
         //check if this combination is already seen ?
         it = COMB_SET.find(*(C->key));

         C->parents[C->parents_len++] = i;

         if(it!=COMB_SET.end())
         {
            if(VERBOSE)
                printf("\t*** Combination %s already present, skipping\n",C->key->c_str());
            remove(C,snp2);
            C->parents_len--;
            continue;
         }

         if(curr_block < 0)
         {
           curr_chr = snp2->chr;
           curr_block = snp2->LD_block_id;
         }
        
         if(order<=2 && phenotype->len <=2)
            createTable1(snp_queue,C,phenotype); //should NOT malloc
         else
            createTable2(snp_queue,C,phenotype); //should NOT malloc
         double pai_value = pai(C, phenotype->entropy, phenotype->nsample);

         double df = (C->table->dim_P[0]-1)*(C->table->dim_P[1]-1);
         C->pai_pvalue = gsl_cdf_gamma_Q(C->pai, df/2.0, scale);
         if(VERBOSE)
         {
            printf("Computed ->");print(C);//print(C->table);
         }

         if(snp2->LD_block_id < 0) //no block information
         {
            if(bestC->pai>0)
            {
               COMBINATION * Ccopy = combination(bestC);//malloc to store.
               push(PQ,Ccopy);
               bestC->pai = 0;
               bestC->pai_pvalue = 1.0;
               if(VERBOSE)
               {
                  printf("\tStored 3 -> ");print(Ccopy);
               }
               COMB_SET.insert(*(Ccopy->key));
            }
            if(C->pai_pvalue < PAI_PVAL_CUTOFF)
            {
               COMBINATION * Ccopy = combination(C);//malloc to store.
               if(VERBOSE)
               {
                  printf("block = -1, stored "); print(C);
               }
               push(PQ,Ccopy);
               COMB_SET.insert(*(Ccopy->key));
            }
         }
         else if(curr_block==snp2->LD_block_id && curr_chr==snp2->chr)
         {
           if(C->pai_pvalue < PAI_PVAL_CUTOFF)
           {
             if(pai_value > bestpai_block)
             {
               combination_overwrite(bestC,C); //should not malloc
               bestpai_block = pai_value;
               if(VERBOSE)
               {
                  printf("\tUpdated 1 -> ");print(bestC);
               }
               updated = true;
             }
           }
         }
         else
         {
            if(bestC->pai>0 && updated)
            {
              COMBINATION * Ccopy = combination(bestC); //malloc to store in BUFFER.
              if(VERBOSE)
              {
                 printf("\tStored 1 -> ");print(Ccopy);
              }
              push(PQ,Ccopy);
              COMB_SET.insert(*(Ccopy->key));
            }

            updated = false;
            bestC->pai = 0;
            bestC->pai_pvalue = 1.0;

            if(C->pai_pvalue < PAI_PVAL_CUTOFF)
            {
              combination_overwrite(bestC,C); //should not malloc
              bestpai_block = pai_value;
              if(VERBOSE)
              {
                 printf("\tUpdated 2 -> ");print(bestC);
              }
              curr_block = snp2->LD_block_id;
              curr_chr = snp2->chr;
              updated = true;
            }
         }
         remove(C,snp2);
         C->parents_len--;

         if(count>=gap)
         {
            long double completed = (long double)count_tot*100/est_pairs;
            printf("Fraction %llf completed",completed);
            fflush(stdout);
            if(!VERBOSE)
               printf("\r");
            count_tot += count;
            count = 0;
         } 
      }
      if(bestC->pai_pvalue < PAI_PVAL_CUTOFF && updated==true)
      {
        COMBINATION * Ccopy = combination(bestC); //malloc to store in BUFFER.
        if(VERBOSE)
        {
           printf("\tStored 2 -> ");print(Ccopy);
        }
        push(PQ,Ccopy);
        COMB_SET.insert(*(Ccopy->key));
      }

      if(VERBOSE)
         printf("----------------------------------------------------------------------------------------------------\n");
      bestC->pai = 0;
      bestC->pai_pvalue = 1.0;
   }

   printf("Appending %d elements to buffer of size %d\n",PQ->size(),BUFFER->size());
   //append to BUFFER.
   while(!PQ->empty())
   {
      COMBINATION * c = PQ->top();
      BUFFER->push_back(c);
      //COMB_SET.insert(*(c->key));
      PQ->pop();
   }
   printf("New buffer size = %d\n",BUFFER->size());
   delete PQ;

   COMB_SET.clear();
   free_combination(C);
   free_combination(bestC);
   return end;
}

char *itob(unsigned short x)
{
  //int SZ = sizeof(unsigned short);
  static char buff[MAX_COMB_SIZE + 1];
  int i;
  int j = MAX_COMB_SIZE - 1;

  buff[j] = 0;
  for(i=0;i<MAX_COMB_SIZE; i++)
  {
     if(x & ((unsigned short)1 << i))
        buff[j] = '1';
     else
        buff[j] = '0';
     j--;
  }
  return buff;
}

string get_string(COMBINATION * C, unsigned short x)
{
   static char s[MAX_COMB_CHARS] = "";
   char * z = itob(x);
   cout<<"z = "<<z<<endl;
   int k = 0;
   sprintf(s,"");
   for(int i=MAX_COMB_SIZE-1;i>=0;i--)
   {
      if(z[i]=='1')
      {
         if(k<C->len)
           sprintf(s,"%s %d ",s,C->snps[k]->id);
         else
           sprintf(s,"%s P",s);
      }
      k++;
   }
   return string(s);
}

void print(vector<int> v, COMBINATION * C)
{
   for(int i=0;i<v.size();i++)
   {
      if(v[i]<C->len)
        cout<<"index = "<<v[i]<<"	:  snps = "<<C->snps[v[i]]->id<<endl;
      else
        cout<<"index = "<<v[i]<<"	:  P"<<endl;
   }
   cout<<"...."<<endl;
}

void permute_table(TABLE * tab)
{
  int key = 0;
  run_ASA159(tab->rowsum, tab->colsum, &key, tab, &SEED);
  //cout<<"After permutation : "<<endl;
  //print(tab);
}

void compute_kwii(vector<COMBINATION *> * BUFFER, SNP_QUEUE* snp_queue, PHENOTYPE * phenotype, FILE * fp_kwii)
{
   int N = BUFFER->size();
   int flag[N];
   for(int i=0;i<N;i++)
      flag[i] = 0;//if 1, do not compute kwii of that combination.

   TABLE * T = NULL;
   TABLE * Tab = NULL;
   vector<int> subset;
   int n_max = (int)pow(2.0,MAX_COMB_SIZE+1);//+1 for phenotype
   int pheno_present_array[n_max];
   vector<int> ARRAY[n_max]; //array of empty vectors.
   vector<string> SUBSETS; //empty vector 
   double H[n_max];
   KWII_PERM kwii_perm[n_max];
   int last_size = -1;

   tr1::unordered_set<string> KWII_SET;
   tr1::unordered_set<string>::iterator KWII_SET_it;

   for(int i=N-1;i>=0;i--)
   {
      COMBINATION * C = (*BUFFER)[i];
      //printf("            KWII for ");print_key(C);
      //cout<<"index = "<<i<<" : KWII for ";
      //print_key(C);
      //do not compute kwii for this combination as a child is already computed.
      //also set all parent computations off.
      for(int j=0;j<C->parents_len;j++)
      {
         int parent_index = C->parents[j];
         flag[parent_index] = 1;
      } 

      if(flag[i])
      {
         //cout<<"Skipping ...\n";
         continue;
      }

      if(!T) //table should be big enough as we started with the biggest combination.
      {
         int max_rows = (int)pow(3.0,C->table->dim_T[1]);
         T = table(max_rows, C->table->dim_T[1], max_rows, C->table->dim_P[1]);//malloc
         Tab = table(max_rows, C->table->dim_T[1], max_rows, C->table->dim_P[1]);//malloc
      }
      //let the combination snps are indexed as 0,1,2,3,...,len-1. 
      //generate all possible subsets of 0,1,2,3,...,len-1,len. Index len indicates the phenotype.
      //compute the entropies of each subset.
      //For each subset, if it is a parent, set flag to 1 for the parent.

      if(last_size>0)
      {
        for(int j=0;j<last_size;j++)
           ARRAY[j].clear();
      }
      SUBSETS.clear();
      memset(pheno_present_array,0,n_max);
   
      int n = (int)pow(2.0,C->len+1);
      last_size = n;
      //From the rhs : position 0 bit for snp1
      //From the rhs : position 1 bit for snp2
      //From the rhs : position 2 bit for snp3 ... 
      //e.g. say combination = (snp1 snp2 snp3 P) 
      //subset = (snp2 P) = 0000 1010
      //subset = (snp2 snp3 P) = 0000 1110
      //subset = (snp1 snp2 snp3) = 0000 0111
      
      char s[20];
      table(Tab,C->table); //Tab = C->table

      for(int perm=0;perm<=N_PERM;perm++) //perm = 0 for true Table. After that permute the table, marginalise and compute entropy and kwii for each perm.
      {
        if(VERBOSE)
           cout<<"PERM "<<perm<<"out of "<<N_PERM<<endl;
        if(perm==0)
           SUBSETS.push_back("");//for x==0
        else
           permute_table(Tab);

        H[0] = 0;//for x==0
        for(unsigned short x=1;x<n;x++)
        {
          bool pheno_present = false;
          if(perm==0)
          {
            string key = "";
            for(int j=0;j<=C->len;j++) //last position is for phenotype.
            {
              if(x & ((unsigned short)1 << j))
              {
                ARRAY[x].push_back(j);//indices of snps stored.
                if(j==C->len)
                {
                  if(ARRAY[x].size()==1)
                    key.append("P");
                  else
                    key.append(":P");
                  pheno_present = true;
                }
                else
                {
                  if(ARRAY[x].size()==1)
                    sprintf(s,"%d",C->snps[j]->id);
                  else
                    sprintf(s,":%d",C->snps[j]->id);
                  key.append(s);
                }
              }
            }
            SUBSETS.push_back(key);
            pheno_present_array[x] = pheno_present;
          }
          else
             pheno_present = pheno_present_array[x];

          double ent = -1;
          if(pheno_present)
          {
            if(ARRAY[x].size()>1)
            {
               marginalise(Tab, C->len, ARRAY[x], phenotype, false, T); //do not marginalise over phenotypes.
               ent = entropy(T,phenotype->nsample);
            }
            else
               ent = phenotype->entropy;
          }
          else
          {
            marginalise(Tab, C->len, ARRAY[x], phenotype, true, T); //marginalise over phenotypes.
            ent = entropy(T,phenotype->nsample);
          }
          H[x] = ent;
        }

        for(int elem=0;elem<C->len+1;elem++)
        {
          unsigned short u = ((unsigned short)1 << elem);
          for(unsigned short x=1;x<n;x++)
          {
            unsigned short ux = (u & x);
            if(ux==0)
            {
              //x does not have the bit corresponding to elem set.
              //so x is for a subset without elem.
              unsigned short z = (u | x);
              H[z] -= H[x];
            }
          }
        }

        bool printed = false;
        for(unsigned short x=1;x<n;x++)
        {
           double kwii = -H[x];
           if(perm==0)
           {
              if(VERBOSE)
                 cout<<"kwii(real) "<<SUBSETS[x]<<" = "<<kwii<<endl;
              kwii_perm[x].kwii = kwii;
              kwii_perm[x].perms = 0;
              kwii_perm[x].hits = 0;
           }
           else
           {
              kwii_perm[x].perms++;
              if(kwii >= kwii_perm[x].kwii)
                kwii_perm[x].hits++;
              if(VERBOSE)  
                cout<<"kwii(perm) "<<SUBSETS[x]<<" = "<<kwii<<"hits = "<<kwii_perm[x].hits<<" perms = "<<kwii_perm[x].perms<<endl;
           }

        }
      }//end for each perm.

      bool printed = false;
      for(unsigned short x=1;x<n;x++)
      {
         double kwii = kwii_perm[x].kwii;
         if(kwii < 0 && !SHOW_NEG_KWII)
            continue;

         KWII_SET_it = KWII_SET.find(SUBSETS[x]);
         if(pheno_present_array[x]==1 && KWII_SET_it==KWII_SET.end())
         {
             if(fp_kwii!=NULL)
               fprintf(fp_kwii,"%s\t%g%g\n",SUBSETS[x].c_str(),kwii,(double)kwii_perm[x].hits/kwii_perm[x].perms); 
             KWII_SET.insert(SUBSETS[x]);
             printf("%s = %g %d %d\n",SUBSETS[x].c_str(),kwii,kwii_perm[x].hits,kwii_perm[x].perms);
             printed = true;
         }
      }

      if(printed)
         cout<<"*******************************\n";
   }   
}
