#ifndef _IO_H
#define _IO_H

#include "FastEpi.h"
#include "SQueue.h"
#include "Compress.h"

void getFilename(char* filename, char* dir, char* name, char* fix);
char* getTime();
PHENOTYPE * readPhenotype(char* filename);
int readGeno( char* name, int * chr, double * pos, int * bp, double * MAF, int * geno, int nsample, char * sline_geno);
void createSNP(SNP_type* snp, int * geno, char* name, int bp, int chr, double MAF,int nsample);
SNP_type* readSNP(FILE * fp_tped, int nsample);
bool file_exists(const char *filename);
int readGene(FILE * fp, GENE_type** gene, int SNPchr);
void assignSNP2Gene(GENE_type** startGene, SNP_type * snp, int curSNP_id);
int parseArgs(int nARG, char* ARGV[], PAR* par);
void free_genes(GENE_type*);
void free_phenotype(PHENOTYPE*);
void free_snp(SNP_type*);
void free_queue(SNP_QUEUE*);
#endif
