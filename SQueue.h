#ifndef _SQUEUE_H
#define _SQUEUE_H

#include "FastEpi.h"

SNP_QUEUE * nq_init(int);
void nq_push(SNP_QUEUE* n_queue,SNP_type* SNP);
SNP_type * nq_pop(SNP_QUEUE * n_queue);
SNP_type * nq_get(SNP_QUEUE * n_queue, int index);
void nq_clean(SNP_QUEUE * n_queue);
#endif
