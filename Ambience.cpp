#include "FastEpi.h"
#include "Compress.h"
#include "IO.h"
#include "Combination.h"
#include "Table.h"
#include "Stat.h"

int get_snp_count(char* fname)
{
    char command[100] = "";
    sprintf(command,"sed -n '$=' %s",fname);
    FILE * fp = popen(command,"r");
    int n = 0;
    fscanf(fp,"%d",&n);
    printf("# snps = %d\n",n);
    return n;
}

int get_ind_count(char* fname)
{
    char command[100] = "";
    sprintf(command,"wc -l %s",fname);
    FILE * fp = popen(command,"r");
    int n = 0;
    fscanf(fp,"%d",&n);
    printf("# samples = %d\n",n);
    return n;
}

GENE_type * read_genes(char * gene_file)
{
  FILE *fp;
  fp = fopen(gene_file, "r");
  GENE_type* gene = NULL;
  for(int chr=1;chr<=22;chr++)
  {
     int nGene = readGene(fp, &gene, chr);
     if(nGene>0)
        printf("-%d genes read for chr %d\n",nGene,chr);
  }
  return gene;
}

void create_block(Block_type ** tail, SNP_type * snp)
{
   static int ID = 0;
   if(*tail==NULL)
   {
      *tail = (Block_type*)malloc(sizeof(Block_type));
      (*tail)->id = ID;
      (*tail)->chr = snp->chr;
      (*tail)->block_id = snp->LD_block_id;
      (*tail)->snp_start = snp->id;
      (*tail)->snp_end = snp->id;
      (*tail)->next = NULL;
      //printf("Created block %d : %d-%d\n",(*tail)->id,snp->id,snp->id); 
   }
   else if(snp->LD_block_id < 0) //no block info, assign a block.
   {
      Block_type * B = (Block_type*)malloc(sizeof(Block_type));
      B->chr = snp->chr;
      B->block_id = snp->LD_block_id;
      B->snp_start = snp->id;
      B->snp_end = snp->id;
   
      (*tail)->next = B;
      B->next = NULL;
      (*tail) = B;
      (*tail)->id = ++ID;
      //printf("Created block %d : %d-%d\n",(*tail)->id,snp->id,snp->id); 
   }
   else if(snp->chr!=(*tail)->chr || snp->LD_block_id!=(*tail)->block_id)
   {
      Block_type * B = (Block_type*)malloc(sizeof(Block_type));
      B->chr = snp->chr;
      B->block_id = snp->LD_block_id;
      B->snp_start = snp->id;
      B->snp_end = snp->id;
   
      (*tail)->next = B;
      B->next = NULL;
      (*tail) = B;
      (*tail)->id = ++ID;
      //printf("Created block %d : %d-%d\n",(*tail)->id,snp->id,snp->id); 
   }
   else
   {
      if(ID!=(*tail)->id)
      {
         printf("Error in block ids %d != %d\n",ID,(*tail)->id);
         exit(0);
      }
      (*tail)->snp_end = snp->id;
      //printf("Updated block %d : %d-%d\n",(*tail)->id,(*tail)->snp_start,(*tail)->snp_end); 
   }
   //printf("<<< %d >>>>\n",(*tail)->id);    
}

void printBlock(Block_type * start)
{
   Block_type * tail = start;
   while(tail)
   {
      printf("   Block = %d : %d - %d\n",tail->id,tail->snp_start,tail->snp_end);
      tail = tail->next;
   }
}

SNP_QUEUE* read_data(char* tped_file, GENE_type * Genes, Block_type ** startBlock, int nsample, int nsnps)
{
  //open input file
  FILE *fp_tped = NULL;
  fp_tped = fopen(tped_file, "r");

  GENE_type * startGene = NULL;
  if(Genes)
     startGene = Genes->next;

  Block_type * tailBlock = NULL;

  n_chunk = (int)ceil((double)nsample/CHUNK_SZ);

  SNP_QUEUE * snp_queue =  nq_init(nsnps);
  SNP_type* curSNP= NULL;
  int curSNP_id = 0;

  printf("Starting to read %d snps....\n",nsnps);

  int i = 1;
  int blockid = 0;
  curSNP = readSNP(fp_tped,nsample);
  if(FILTER_BY_BLOCK)
     curSNP->LD_block_id = blockid;

  SNP_type * oldSNP = NULL;

  while(curSNP!=NULL)
  {
     if(FILTER_BY_BLOCK)
     {
        create_block(&tailBlock,curSNP);
        if(!(*startBlock))
          *startBlock = tailBlock;
     }

     if(Genes)
       assignSNP2Gene(&startGene, curSNP,curSNP_id);

     if(curSNP!= NULL)
     {
       //printf("Read line = %d\n",i);
       nq_push(snp_queue,curSNP);
       curSNP_id++;
     }
     oldSNP = curSNP;
     curSNP = readSNP(fp_tped,nsample);
  
     if(oldSNP!=NULL && curSNP!=NULL)
     {
        if(FILTER_BY_BLOCK)
        {
          double r2 = sq_correlation(curSNP->geno,oldSNP->geno,nsample); //squared correlation with the previous snp.
          if(r2>BLOCK_CUTOFF)
             curSNP->LD_block_id = blockid;
          else
          {
             blockid++;
             curSNP->LD_block_id = blockid;
          }
          //cout<<oldSNP->name<<","<<curSNP->name<<" = "<<r2<<endl;
          //cout<<oldSNP->LD_block_id<<","<<curSNP->LD_block_id<<endl;
          //cout<<"----------------------------------\n";
        }
        free(oldSNP->geno);
        oldSNP->geno = NULL;
     }
     i++;
  }

  if(oldSNP!=NULL)
  {
     free(oldSNP->geno);
     oldSNP->geno = NULL;
  }
  fclose(fp_tped);
  if(FILTER_BY_BLOCK)
  {
     //printBlock(*startBlock);
  }
  return snp_queue;
}

int main(int nARG, char *ARGV[])
{
  gene_based = true; //consider only gene based snps ?
  PAR par;
  parseArgs(nARG, ARGV, &par);
  char * tped_file = par.tped_file;//"./Data/chr10.amb.tped";
  char * genes_file = par.genes_file;//"./Data/chr10.genes.txt";
  char * pheno_file = par.pheno_file;//"./Data/chr10.tfam";

  //Always check these for memory problems.
  int N = get_ind_count(pheno_file);
  int nsnps = get_snp_count(tped_file);

  char out_pai_file[100]; //"./Out1/pai.txt";
  char out_kwii_file[100];  //"./Out1/kwii.txt";

  sprintf(out_pai_file,"%s.pai.txt",par.out_file);
  sprintf(out_kwii_file,"%s.kwii.txt",par.out_file);

  FILE * fp_pai = fopen(out_pai_file, "w");
  FILE * fp_kwii = fopen(out_kwii_file, "w");

  GENE_type * Genes = NULL;
  if(genes_file!=NULL)
  {
     Genes =  read_genes(genes_file);
  }
  Block_type * Blocks = NULL;
  SNP_QUEUE* snp_queue = read_data(tped_file,Genes,&Blocks,N,nsnps);
  PHENOTYPE * pheno = readPhenotype(pheno_file);
  vector<COMBINATION*> BUFFER;

  /* 
  if(0)
  {
    COMBINATION * C1 = combination();
    append(C1,nq_get(snp_queue,0));
    append(C1,nq_get(snp_queue,2));
    append(C1,nq_get(snp_queue,3));

    COMBINATION * C2 = combination();
    append(C2,nq_get(snp_queue,5));
    append(C2,nq_get(snp_queue,8));
    append(C2,nq_get(snp_queue,9));

    COMBINATION * C3 = combination();
    append(C3,nq_get(snp_queue,1));
    append(C3,nq_get(snp_queue,8));

    TABLE * T1 = createTable2(snp_queue,C1,pheno);
    TABLE * T2 = createTable2(snp_queue,C2,pheno);
    TABLE * T3 = createTable2(snp_queue,C3,pheno);

     vector<COMBINATION*> BUFFER;
     BUFFER.push_back(C3);
     BUFFER.push_back(C2);
     BUFFER.push_back(C1);

     //vector<int> cols(1,0);
     //T = table(C1->table->dim_T[0], C1->table->dim_T[1], C1->table->dim_P[0], C1->table->dim_P[1]);
     //marginalise(C1, cols, pheno, false, T);
     compute_kwii(&BUFFER, snp_queue, pheno, NULL);
     //permute_table(T);
  }
  else
  */
  {
     if(!PAIRWISE_START)
     {
       single(snp_queue,pheno,&BUFFER);
       iterate(snp_queue, pheno, &BUFFER, MAX_ITER-1,1,0,fp_pai);
       compute_kwii(&BUFFER, snp_queue, pheno, fp_kwii);
     }
     else
     {
       single(snp_queue,pheno,&BUFFER);
       int start = BUFFER.size();
       pairwise_block(snp_queue,pheno,&BUFFER,Blocks);
       iterate(snp_queue, pheno, &BUFFER, MAX_ITER-2,2, start, fp_pai);
       compute_kwii(&BUFFER, snp_queue, pheno, fp_kwii);
     }
  }

  fclose(fp_pai);
  fclose(fp_kwii);

  free_genes(Genes);
  free_phenotype(pheno);
  free_queue(snp_queue);
}

