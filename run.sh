./Ambience --infile ./Data/chr.tped --phenofile ./Data/trait.tfam --outfile Out/out --pai_pval_cutoff 0.5 --nperm 100 \
           --maxiter 3 --pairwise --genefile ./Data/genes.txt
