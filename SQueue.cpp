#include "SQueue.h"

SNP_QUEUE * nq_init(int n)
{
   SNP_QUEUE * n_queue = (SNP_QUEUE*) malloc(sizeof(SNP_QUEUE));
   n_queue->SNPS = (SNP_type**) malloc(n*sizeof(SNP_type*));
   if(n_queue->SNPS==NULL)
   {
      printf("ERROR : Failed to allocate memory for %d snps, quitting\n",n);
      abort();
   }
   int i = 0;
   for(i=0;i<n;i++)
      n_queue->SNPS[i] = NULL;
   n_queue->head = n_queue->tail = NULL;
   n_queue->size = 0;
   return n_queue;
}

void nq_push(SNP_QUEUE* n_queue,SNP_type* SNP)
{
    if(n_queue->size==0)
       n_queue->head = SNP;
    n_queue->SNPS[n_queue->size] = SNP;
    n_queue->size++;
    n_queue->tail = SNP;
}

SNP_type * nq_pop(SNP_QUEUE * n_queue)
{
    if(n_queue->size>0)
    {
       SNP_type * snp = n_queue->SNPS[n_queue->size-1];
       n_queue->SNPS[n_queue->size-1] = NULL;
       n_queue->size--;
       return snp;
    }
    else
       return NULL;
}

SNP_type * nq_get(SNP_QUEUE * n_queue, int index)
{
    if(n_queue->size>0)
    {
       return n_queue->SNPS[index];
    }
    else
       return NULL;
}

void nq_clean(SNP_QUEUE * n_queue) //free the queue
{
  free(n_queue->SNPS);n_queue->SNPS=NULL;
  free(n_queue);n_queue=NULL;
}
