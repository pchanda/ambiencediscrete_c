#include "IO.h"

int n_chunk;
bool VERBOSE;
double PAI_PVAL_CUTOFF;
int GENE_FLANK;
bool SHOW_NEG_KWII;
int MAX_BUFFER_SIZE;
int MAX_ITER;
int N_PERM;
int SEED;
bool PAIRWISE_START;

void getFilename(char* filename, char* dir, char* name, char* fix){
  strcat(filename, dir);
  strcat(filename, name);
  strcat(filename, fix);
}

char* getTime(){
  time_t raw_time;

  time(&raw_time);

  return ctime(&raw_time);
}

GENE_type* createGene(char* name, int chr, int bp_start, int bp_end){

  static int geneid = -1;//first time called with HEAD.
  if(chr<0)
     geneid = -1;

  GENE_type* gene = (GENE_type*) malloc(sizeof(GENE_type));

  strcpy(gene->name, name);
  gene->bp_start = bp_start;
  gene->bp_end = bp_end;
  gene->chr = chr;
  gene->nSNP = 0;
  gene->gene_id = geneid;
  geneid++;

  gene->next=NULL;
  gene->snp_start = -1;
  gene->snp_end = -1;
  gene->skip = false;

  return gene;
}

int readGene(FILE * fp, GENE_type** gene, int SNPchr)
{
  int chr;
  int bp_start;
  int bp_end;
  int nGene = 0;  
  int prevchr = -1;
  int prevbp = -1;

  char sline[MAX_LINE_WIDTH];
  char name[GENE_ID_LEN];
  fpos_t prevPos;
  GENE_type* curGene=NULL;  
  GENE_type* prevGene = NULL;

  static GENE_type* tail = NULL;

  if(*gene==NULL)
  {
    *gene = createGene("HEAD", -1, -1, -1);
    tail = *gene;
  }
  prevGene = tail;

  //printf("-start to load genes for chr %d \n", SNPchr);
  bool stop = false;
  while(!feof(fp)){
    
    fgetpos(fp, &prevPos);
    strcpy(sline, "");
    fgets(sline, MAX_LINE_WIDTH, fp);
    if(strlen(sline)==0)
      continue;

    if(sline[0] == '#'){
      printf("-skipping header: %s", sline);
      continue;
    }
    int nstatus;
    nstatus= sscanf(sline, "%*s %s %d %d %d",name, &chr, &bp_start, &bp_end);
    if(nstatus == 4 )
    {
      if(prevchr < chr)
        prevchr = chr;
      else if(prevchr > chr){
        printf("-gene file not sorted by chr\n");
        abort();
      }

      if(SNPchr == chr){

        if(prevbp < bp_start)
          prevbp = bp_start;
        else if(prevbp > bp_start){
          printf("-gene file not sorted by bp\n");
          abort();
        }

        //printf("Creating gene %s, prev gene = %s\n",name,prevGene->name);
        curGene= createGene(name, chr, bp_start, bp_end);
        prevGene->next = curGene;
        prevGene = curGene;

        nGene++;
      }
      else
      {
        //new chromosome start.
        stop = true;
        fsetpos(fp, &prevPos);
      }
    }else{
      printf("-Error in reading gene : %s", sline);
      exit(0);
    }
    if(stop)
       break;
  }
  //printf("-done loading genes for chr %d \n",SNPchr);
  tail = prevGene;
  return nGene;
}

// assign SNPs to gene
void assignSNP2Gene(GENE_type** startGene, SNP_type * snp, int curSNP_id)
{
  //printf("\nTry : %s %d start search with %s\n",snp->name,snp->chr,(*startGene)->name); 
  GENE_type* gene = *startGene;
  bool firstRun = true;

  for(; gene != NULL; gene = gene->next)
  {
    //printf("looking at : %s\n",gene->name);
    if(gene->skip) continue;
    int downstream = gene->bp_end + GENE_FLANK;
    int upstream = gene->bp_start - GENE_FLANK;

    if(snp == NULL){
      gene->skip =true;
      continue;
    }

    if(snp->chr > gene->chr)
    {
       //printf("snp chr %d > gene chr %d\n",snp->chr,gene->chr);
       continue;
    }

    if(gene->chr > snp->chr)
    {
       //printf("snp chr %d < gene chr %d\n",snp->chr,gene->chr);
       break;
    }

    //upstream is not decreasing
    if(snp->bp <upstream){
      break;
    }
    else if(snp->bp > downstream){
      gene->skip =true;
    }
    else
    {
      if(firstRun)
      {
          firstRun = false;
          *startGene = gene; //set startGene to the first gene this snp is mapped to.
      }
      if(snp->MAF >= 0.1)
      { 
        if(gene->snp_start == -1){
          gene->snp_start = curSNP_id;
          gene->nSNP = 0;
        }
        if(snp->snp_genes==NULL)
        {
           snp->snp_genes = (SNP_GENES_type*) malloc(sizeof(SNP_GENES_type));
           snp->snp_genes->gene = gene;
           snp->tail = snp->snp_genes;
           snp->snp_genes->next = NULL;
        }
        else
        {
           snp->tail->next = (SNP_GENES_type*) malloc(sizeof(SNP_GENES_type));
           snp->tail = snp->tail->next;
           snp->tail->gene = gene;
           snp->tail->next = NULL;
        }
        //printf("Assign : %s -> %s\n",snp->name,gene->name); fflush(stdout);
        gene->snp_end = curSNP_id;
        gene->nSNP++;
        snp->nGene++;
      }
      else
      {
         //printf("Omit : \t%s\t%d\t%d\t%f\n",snp->name,snp->chr,snp->bp,snp->MAF);
      }
    }
  }
}

PHENOTYPE * readPhenotype(char* filename)
{
  FILE *fp;
  char s_phenotype[PHENOTYPE_VALUE_LEN];
  char sline[MAX_LINE_WIDTH];

  PHENOTYPE * phenotype = (PHENOTYPE*) malloc(sizeof(PHENOTYPE));
 
  for(int i=0;i<MAX_PHEN_VALUE;i++)
    phenotype->values[i] = 0;

  int len = -10000;

  phenotype->nsample = 0;
  phenotype->data = (int*)malloc(MAX_N_INDIV*sizeof(int));

  fp = fopen(filename, "r");
  int i = 0;
  while(!feof(fp))
  {
    strcpy(sline, "");
    fgets(sline, MAX_LINE_WIDTH, fp);

    //skip header line
    if(sline[0]=='#') continue;
    if(strcmp(sline,"")==0) continue;

    int nstatus = sscanf(sline,"%*s %*s %*s %*s %*s %s\n", s_phenotype);

    if(sscanf(s_phenotype, "%d", &(phenotype->data[i])) != 1)
    {
       printf("-Wrong format in phenotype %d: %s\n", phenotype->nsample, sline);
       exit(1);
    }
    if(phenotype->data[i]<0) 
    {
       printf("phenotype value must be consecutive integers starting 0 and upto %d\n (e.g. 0 1 2) and cannot be negative\n",MAX_PHEN_VALUE); 
       exit(1);
    }
    phenotype->values[phenotype->data[i]]++;
    if(phenotype->data[i]>len)
      len = phenotype->data[i];
    (phenotype->nsample)++;
    //printf("Read ph=%d\n",phenotype->data[i]);
    i++;
    //printf("Got [%s], nsample=%d\n",sline,phenotype->nsample);
  }
  fclose(fp);
  phenotype->len = len+1;
  printf("This phenotype has %d distinct values\n",phenotype->len);
  if(phenotype->len<=2)
  {
    printf("Compression binary phenotype\n");
    phenotype->C0 = (chunk_t*)malloc(n_chunk*sizeof(chunk_t));
    phenotype->C1 = (chunk_t*)malloc(n_chunk*sizeof(chunk_t));
    compress_pheno(phenotype);
  }
  double Hpheno = 0;
  for(int i=0;i<phenotype->len;i++)
  {
     double prob = (double)phenotype->values[i]/phenotype->nsample;
     if(prob<=0)
     {
       printf("Error in phenotype values : value %d not found\n",i);
       exit(0);
     }
     if(prob>0)
        Hpheno -= (prob) * log(prob);
  }
  phenotype->entropy = Hpheno;
  printf("Phenotype entropy = %g\n",Hpheno);
  return phenotype;
}

//read a line from the genotype file, format the data and save it in the SNP data structure
int readGeno( char* name, int * chr, double * pos, int * bp, double * MAF, int * geno, int nsample, char * sline_geno)
{
  int i;
  char * nextGeno;
  
  int k = 0;//indexes genotypes read.
  int first_geno;
  bool monomorphic = true;

  //for(i=0;i<10;i++) printf("[%c] ",sline_geno[i]); printf("\n");

  //read chr
  nextGeno = strchr(sline_geno, '\t');
  if(nextGeno!=NULL) *nextGeno='\0'; 
  //printf("line=[%s]\n",sline_geno);
  sscanf(sline_geno, "%d", chr);
  sline_geno = nextGeno+1;
  //printf("Read chr = %d\n",*chr);

  //read snp name
  nextGeno = strchr(sline_geno, '\t');
  if(nextGeno!=NULL) *nextGeno='\0';
  //printf("line=[%s]\n",sline_geno);
  sscanf(sline_geno, "%s", name);
  //printf("Read snp = %s\n",name);
  sline_geno = nextGeno+1;

  //read cm
  double cm;
  nextGeno = strchr(sline_geno, '\t');
  if(nextGeno!=NULL) *nextGeno='\0';
  //printf("line=[%s]\n",sline_geno);
  sscanf(sline_geno, "%lg", &cm);
  //printf("Read cm = %lg\n",cm);
  sline_geno = nextGeno+1;

  //read bp
  nextGeno = strchr(sline_geno, '\t');
  if(nextGeno!=NULL) *nextGeno='\0';
  sscanf(sline_geno, "%d", bp);
  //printf("Read bp = %d\n",*bp);
  sline_geno = nextGeno+1;

  *MAF = 0;
 
  for (i=0; i<nsample; i++)
  {
    nextGeno = strchr(sline_geno, '\t');
    //truncate the string so that sscanf runs much faster
    if(nextGeno!=NULL) *nextGeno='\0';
    if(sscanf(sline_geno, "%d", geno) != 1){
      printf("-Error in tped file: genotype at sample %d\n",i);
      abort();
    }
    if(nextGeno!=NULL)
      sline_geno = nextGeno+1;
	
    if(k==0)
    {
      first_geno = *geno;
    }

    if(monomorphic)
    {
       if(*geno!=first_geno)
       {
          monomorphic = false;
       }
    }
    (*MAF) += (*geno);
    geno++;
    k++;
  }
  (*MAF) /= (2*k);
  if(monomorphic)
    return -1;
  return 1;
}

void createSNP(SNP_type* snp, int * geno, char* name, int bp, int chr, int nsample, double maf)
{
  static int id = 0;
  
  //printf("here\n");fflush(stdout);
  strcpy(snp->name, name);
 
  snp->id = id;	
  snp->bp=bp;
  snp->chr=chr;
  snp->MAF=maf;
  snp->geno = geno;
  snp->nsample = nsample;
  snp->C0 = (chunk_t*)malloc(n_chunk*sizeof(chunk_t));
  snp->C1 = (chunk_t*)malloc(n_chunk*sizeof(chunk_t));
  snp->C2 = (chunk_t*)malloc(n_chunk*sizeof(chunk_t));
  snp->LD_block_id = -1;
  compress_snp(geno,snp);
  snp->snp_genes = NULL;
  snp->tail = NULL;
  snp->nGene = 0;
  id++;
}

//read SNP from file
SNP_type* readSNP(FILE * fp_tped, int nsample)
{
  static char sline_geno[MAX_LINE_WIDTH];
  static char snp_id[SNP_ID_LEN];
  static int chr;
  static int bp;
  static double pos;
  static double MAF;

  // to skip blank lines
  while(!feof(fp_tped))
  {
    strcpy(sline_geno, "");
    fgets(sline_geno, MAX_LINE_WIDTH, fp_tped);
	  
    if(strlen(sline_geno) > 0)
    {
      int * geno = (int*)malloc(sizeof(int)*MAX_N_INDIV);
      int status =  readGeno(snp_id, &chr, &pos, &bp, &MAF, geno, nsample, sline_geno);

      if(status >0 ) 
      {
        SNP_type* snp = (SNP_type*)malloc(sizeof(SNP_type));
	createSNP(snp, geno, snp_id, bp,chr,nsample, MAF);
        //free(geno);
        //snp->geno = NULL;
        //geno = NULL;
	return snp;
      }
      else
      {
        if(status==-1)
           printf("Skipping monomorphic snp %s \n",snp_id);
        free(geno);
        geno=NULL;
      }
    }
  }
  //printf("************readSNP : end\n");fflush(stdout);
  return NULL;
}

void print_help()
{
 cout<<"**********Ambience discrete help*************\n";
 cout<<"Run as: ./Ambience options ... \n";
 cout<<"          Options :\n"; 
 cout<<"--infile <genotype file name>         : mandatory\n";
 cout<<"--phenofile <phenotype file name>     : mandatory\n";
 cout<<"--outfile <output file name>          : mandatory\n";
 cout<<"--genefile <genes file name> \n";
 cout<<"--buffer <buffer size>                : default 50\n";
 cout<<"--nperm <no of permutations>          : default 1000\n";
 cout<<"--maxiter <max combination size>      : default 2\n";
 cout<<"--flank <gene flanks>                 : default 20000 base pairs\n";
 cout<<"--show_neg_kwii , show -ve kwii ?     : default false\n";
 cout<<"--pai_pval_cutoff <pai pvalue cutoff> : default 0.01\n";
 cout<<"--pairwise , do all pairwise snp-snp  : default false\n";
 cout<<"*********************************************\n";
}

void init()
{
   gene_based = false;
   VERBOSE = false;
   PAI_PVAL_CUTOFF = 0.01;
   GENE_FLANK = 20000;
   SHOW_NEG_KWII = false;
   MAX_BUFFER_SIZE = 50;
   MAX_ITER = 2;
   N_PERM = 1000;
   SEED = 1234567;
   PAIRWISE_START = false;
}

int parseArgs(int nARG, char* ARGV[], PAR* par)
{
  par->tped_file = NULL;
  par->pheno_file = NULL;
  par->genes_file = NULL;
  par->out_file = NULL;

  int i;
  init();
  bool infile_present = false;
  bool outfile_present = false;
  bool phenofile_present = false;

  for (i=1; i<nARG; i++)
  {
    bool recog = false;

    if(ARGV[i][0]=='#') continue;

    if(strcmp(ARGV[i], "--help") == 0)
    {
       print_help();
       exit(0);
    }

    if(strcmp(ARGV[i], "--verbose") == 0)
    {
       printf("\t--verbose set, useful for debugging purpose\n");
       VERBOSE = true;
       recog = true;
    }

    if(strcmp(ARGV[i], "--infile") == 0)
    {
      if(i+1>=nARG){printf("-Not sufficient arguments, quitting\n");exit(1);}
      par->tped_file = ARGV[++i];
      printf("\t%s: %s\n", "--infile",  par->tped_file );
      recog = true;
      infile_present = true;
    }

    if(strcmp(ARGV[i], "--outfile") == 0)
    {
      if(i+1>=nARG){printf("-Not sufficient arguments, quitting\n");exit(1);}
      par->out_file = ARGV[++i];
      printf("\t%s: %s\n", "--outfile",  par->out_file );
      recog = true;
      outfile_present = true;
    }

    if(strcmp(ARGV[i], "--phenofile") == 0)
    {
      if(i+1>=nARG){printf("-Not sufficient arguments, quitting\n");exit(1);}
      par->pheno_file = ARGV[++i];
      printf("\t%s: %s\n", "--phenofile",  par->pheno_file );
      recog = true;
      phenofile_present = true;
    }

    if(strcmp(ARGV[i], "--genefile") == 0)
    {
      if(i+1>=nARG){printf("-Not sufficient arguments, quitting\n");exit(1);}
      par->genes_file = ARGV[++i];
      printf("\t%s: %s\n", "--genefile",  par->genes_file );
      gene_based = true;
      recog = true;
    }

    if(strcmp(ARGV[i], "--buffer") == 0)
    {
      if(i+1>=nARG){printf("-Not sufficient arguments, quitting\n");exit(1);}
      sscanf(ARGV[++i], "%d", &par->buffer_size);
      printf("\t%s: %d\n", "--buffer",  par->buffer_size );
      MAX_BUFFER_SIZE = par->buffer_size;
      recog = true;
    }

    if(strcmp(ARGV[i], "--nperm") == 0)
    {
      if(i+1>=nARG){printf("-Not sufficient arguments, quitting\n");exit(1);}
      sscanf(ARGV[++i], "%d", &par->nperm);
      printf("\t%s: %d\n", "--nperm",  par->nperm );
      N_PERM = par->nperm;
      recog = true;
    }

    if(strcmp(ARGV[i], "--maxiter") == 0)
    {
      if(i+1>=nARG){printf("-Not sufficient arguments, quitting\n");exit(1);}
      sscanf(ARGV[++i], "%d", &par->max_iter);
      printf("\t%s: %d\n", "--iter",  par->max_iter );
      MAX_ITER = par->max_iter;
      recog = true;
    }
  
    if(strcmp(ARGV[i], "--flank") == 0)
    {
      if(i+1>=nARG){printf("-Not sufficient arguments, quitting\n");exit(1);}
      sscanf(ARGV[++i], "%d", &par->flank);
      printf("\t%s: %d\n", "--iter",  par->flank );
      GENE_FLANK = par->flank;
      recog = true;
    }
  
    if(strcmp(ARGV[i], "--show_neg_kwii") == 0)
    {
      printf("\t%s: true\n", "--show_neg_kwii" );
      SHOW_NEG_KWII = true;
      recog = true;
    }
  
    if(strcmp(ARGV[i], "--pairwise") == 0)
    {
      printf("\t%s: true\n", "--pairwise" );
      PAIRWISE_START = true;
      recog = true;
    }

    if(strcmp(ARGV[i], "--pai_pval_cutoff") == 0)
    {
      if(i+1>=nARG){printf("-Not sufficient arguments, quitting\n");exit(1);}
      sscanf(ARGV[++i], "%lg", &par->pai_pval_cutoff);
      printf("\t%s: %lg\n", "--pai_pval_cutoff",  par->pai_pval_cutoff );
      PAI_PVAL_CUTOFF = par->pai_pval_cutoff;
      recog = true;
    }

    if(!recog)
    {
       printf("Unrecognised option %s: do ./Ambience --help\n",ARGV[i]);
       exit(1);
    }
  }
  
  if(nARG <=1)
  {
     printf("No options provided\n");
     print_help();
     exit(0);
  }

  if(!infile_present) { printf("Missing genotype file, use --infile to provide input genotype file\n");exit(0);}  
  if(!outfile_present) { printf("Missing output file, use --outfile to provide output file\n");exit(0);}  
  if(!phenofile_present) { printf("Missing penotype file, use --phenofile to provide pheno file\n");exit(0);}  

  return 0;
}

// check file existance
bool file_exists(const char *filename){
  FILE *file;
  if ((file = fopen(filename, "r"))) //I'm sure, you meant for READING =)
    {
      fclose(file);
      return true;
    }
  return false;
}

void free_genes(GENE_type * gene)
{
  printf("-cleaning gene queue\n");
  //clean up snps in genes.
  GENE_type *g = gene;
  while(g!=NULL)
  {
     //printf(" cleaning gene %s\n",g->name);
     GENE_type * n = g->next;
     free(g);
     g = n;
  }
}

void free_phenotype(PHENOTYPE * P)
{
   printf("-cleaning phenotype\n");
   free(P->data);
   if(P->len<=2)
   {
     free(P->C0); P->C0 = NULL;
     free(P->C1); P->C1 = NULL;
   }
   free(P);P=NULL;
}

void free_snp(SNP_type * snp)
{
   free(snp->C0);
   free(snp->C1);
   free(snp->C2);
   SNP_GENES_type * g = snp->snp_genes;
   while(g!=NULL)
   {
      SNP_GENES_type * n = g->next;
      free(g);
      g = n;
   }
}

void free_queue(SNP_QUEUE * q)
{
   printf("-cleaning snp queue\n");
   for(int i=0;i<q->size;i++)
   {
      SNP_type * snp = (SNP_type*)nq_get(q,i);
      free_snp(snp);
   }
   nq_clean(q);
}


