#ifndef _STAT_H
#define _STAT_H

#include "FastEpi.h"
#include "Table.h"
#include "SQueue.h"
#include "Combination.h"

double pai(COMBINATION * C,double Hpheno,int nsample);
void single(SNP_QUEUE* snp_queue, PHENOTYPE * phenotype,vector<COMBINATION *>*);
void pairwise_all(SNP_QUEUE* snp_queue, PHENOTYPE * phenotype, vector<COMBINATION*> * BUFFER);
void pairwise_block(SNP_QUEUE* snp_queue, PHENOTYPE * phenotype, vector<COMBINATION*> * BUFFER,Block_type * startBlock);
int higher_order(SNP_QUEUE* snp_queue, PHENOTYPE * phenotype, vector<COMBINATION*> * BUFFER, int order, int start);
void iterate(SNP_QUEUE* snp_queue, PHENOTYPE * phenotype, vector<COMBINATION*> * BUFFER, int max_iter,int order,int str,FILE *);
void compute_kwii(vector<COMBINATION *> * BUFFER, SNP_QUEUE* snp_queue, PHENOTYPE * phenotype, FILE *);
double sq_correlation(int * v1, int * v2, int n);
#endif
